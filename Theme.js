import { ThemeProvider } from "styled-components";
import React from "react";
import PropTypes from "prop-types";

const theme = {
    mainColor: '#eacdc4',
    minorColor: '#898989',
    breakPoints: {
        tablet: {
            media: 1024,
            containerWidth: 720
        },
        desktop: {
            media: 1366,
            containerWidth: 1140
        }
    }
};

// ${({theme}) => `${theme.breakPoints.tablet.media}px`}

export const Theme = ({ children }) => (
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

Theme.propTypes = {
    children: PropTypes.array
};