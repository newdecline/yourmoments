export const routes = [
    {
        href: '/',
        as: '/',
        label: 'Главная'
    },
    {
        href: '/about',
        as: '/about',
        label: 'Будем знакомы'
    },
    {
        href: '/portfolio',
        as: '/portfolio',
        label: 'Портфолио',
        subMenu: [
            {
                label: 'Фото:',
                subMenu: [
                    {
                        href: '/portfolio/[category]',
                        as: '/portfolio/paired',
                        label: 'Парные'
                    },
                    {
                        href: '/portfolio/[category]',
                        as: '/portfolio/wedding',
                        label: 'Свадебные'
                    },
                    {
                        href: '/portfolio/[category]',
                        as: '/portfolio/business-content',
                        label: 'Контент для бизнеса'
                    },
                ]
            },
            {
                href: '/portfolio',
                as: '/portfolio#portfolio-video',
                label: 'Видео',
            },
        ]
    },
    {
        href: '/price',
        as: '/price',
        label: 'Стоимость',
    },
    {
        href: '/training',
        as: '/training',
        label: 'Обучение',
    },
    {
        href:  '/reviews',
        as:  '/reviews',
        label: 'Отзывы',
    },
    {
        href: '/contacts',
        as: '/contacts',
        label: 'Контакты'
    }
];