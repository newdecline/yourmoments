import {BaseStore, getOrCreateStore} from 'next-mobx-wrapper';
import {observable, action} from "mobx";

class AppStore extends BaseStore {
  @observable isOpenMenu = false;

  @observable layoutData = {};

  @observable throughData = {
    phone: '+7 951 791 23 93',
    communicationMethod: {
      vk: 'https://vk.com/lenmontov74photo',
      instagram: 'https://www.instagram.com/lenphoto.ru/',
      mail: 'rocki_lenk@mail.ru'
    }
  };

  @observable aboutImages = [
    '/assets/images/about-slider/001.jpg',
    '/assets/images/about-slider/002.jpg',
  ];

  @action setIsOpenMenu(value) {
    if (value !== false) {
      return this.isOpenMenu = !this.isOpenMenu
    }
    this.isOpenMenu = value
  }

  @action setLayoutData(data) {
    this.layoutData = data
  }
}

export const appStore = getOrCreateStore('appStore', AppStore);