import {BaseStore , getOrCreateStore} from 'next-mobx-wrapper';
import {observable, computed, action} from "mobx";

class PortfolioStore extends BaseStore {
    @observable portfolio = [
        {
            nameSeries: 'paired-photos',
            breadCrumbs: 'Парные',
            poster: 'assets/images/portfolio/paired/poster.jpg',
            series: []
        },
        {
            nameSeries: 'wedding-photos',
            breadCrumbs: 'Свадебные',
            poster: 'assets/images/portfolio/wedding/poster.jpg',
            series: [],
        },
        {
            nameSeries: 'family-photos',
            breadCrumbs: 'Семейные',
            poster: 'assets/images/portfolio/business-content/poster.jpg',
            series: []
        }
    ];

    @action getPhotosSeries(category) {
        return this.portfolio.find(item => item.nameSeries === category);
    }

    @action getPhotoSeries(category, id) {
        const currentCategories = this.portfolio.find(item => item.nameSeries === category);
        return currentCategories.series.find(item => item.id === id);
    }

    @computed get getAllVideos() {
        let videos = [];

        this.portfolio.forEach(categories => {
            categories.series.forEach(series => {
                if (series.videos.length !== 0) {
                    series.videos.forEach(video => {
                        videos.push({title: series.title, video});
                    });
                }
            });
        });
        return videos
    }
}

export const portfolioStore = getOrCreateStore('portfolioStore', PortfolioStore);