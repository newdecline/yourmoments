import styled from "styled-components";

export const StyledVideoPlayer = styled('div')`
    position: relative;
    .video--enabled {
        position: relative;
        padding-bottom: 56.25%;
        iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    }
    .video__link {
        display: block;
        &::after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.3);   
        }
    }
    .video__button {
        position: absolute;
        top: 50%;
        left: 50%;
        border: none;
        padding: 0;
        background-color: transparent;
        transform: translate(-50%, -50%);
        z-index: 1;
        &:hover {
            cursor: pointer;
        }
    }
`;