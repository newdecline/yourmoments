import styled from "styled-components";

export const StyledHeader = styled('header')`
    position: fixed;
    top: 0;
    right: 0;
    width: 100%;
    z-index: 11;
    .wrapper {
        padding: 11px 27px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        background-color:#fff;
        box-sizing: border-box;
        box-shadow: 0 2px 15px rgba(0, 0, 0, 0.1);
    }
    .burger-button {
        width: 42px;
        height: 25px;
        display: flex;
        flex-direction: column;
        align-items: stretch;
        justify-content: space-between;
        background-color: transparent;
        z-index: 12;
        &__item {
            width: 100%;
            height: 3px;
            display: block;
            background-color: ${({theme}) => `${theme.minorColor}`};
            border-radius: 2px;
            transition: opacity .3s, transform .3s, width .3s, background-color .3s;
            transform-origin: center;
            &:nth-child(1) {
                transform: ${({isOpenMenu}) => isOpenMenu ? 'translate(0, 10px) rotate3d(0, 0, 1, 45deg)' : 'translate(0, 0) rotate3d(0, 0, 1, 0)'};
            }
            &:nth-child(2) {
                transform: translate(0, 0);
                opacity: ${({isOpenMenu}) => isOpenMenu ? 0 : 1};
            }
            &:nth-child(3) {
                transform: ${({isOpenMenu}) => isOpenMenu ? 'translate(0, -12px) rotate3d(0, 0, 1, -45deg)' : 'translate(0, 0) rotate3d(0, 0, 1, 0)'};
                width: ${({isOpenMenu}) => isOpenMenu ? '100%' : '70%'};
            }
        }
    }
    .header-phone {
        display: none;
        color: ${({theme}) => `${theme.minorColor}`};
        text-decoration: none;
        font-size: 20px;
        line-height: 29px;
        letter-spacing: 0.09em;
        font-family: 'Circe-Bold', sans-serif;
    }
    .communication-method-list {
        display: none;
    }
    .reserve-date {
        display: block;
        padding: 8px 22px;
        border: 1px solid #898989;
        box-sizing: border-box;
        border-radius: 5px;
        font-size: 18px;
        line-height: 27px;
        text-decoration: none;
        color: ${({theme}) => `${theme.minorColor}`};
        transition: background-color .3s, border-color .3s, color .3s;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .burger-button {
            &:hover {
                cursor: pointer;
                .burger-button__item {
                    background-color: ${({theme}) => `${theme.mainColor}`};
                }
            }
        }
        .wrapper {
            padding: 18px 32px;
        }
        .communication-method-list {
            display: flex;
            align-items: center;
            margin: 0 0 0 43px;
            padding: 0;
            list-style-type: none;
            .item {
              margin-right: 0;
            }
            .link {
              display: flex;
              width: 40px;
              svg {
                height: auto;
              }
              circle {
                fill: transparent;
              }
              path {
                fill: ${({theme}) => `${theme.minorColor}`};
                transition: fill .3s;
              }
            }
        }
        .header-phone {
            position: relative;
            display: block;
            margin: 0 43px 0 auto;
            &::after {
                content: '';
                position: absolute;
                bottom: 0;
                left: 0;
                width: 0;
                display: block;
                height: 2px;
                background-color: ${({theme}) => `${theme.mainColor}`};
                transition: width .3s;
            }
            &:hover {
                &::after {
                    width: 100%;
                }
            }
        }
        .reserve-date {
            &:hover {
                color: #fff;
                background-color: ${({theme}) => `${theme.mainColor}`};
                border-color: ${({theme}) => `${theme.mainColor}`};
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    }
`;
