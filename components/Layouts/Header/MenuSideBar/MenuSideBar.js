import React, {useEffect, useRef} from 'react';
import {StyledMenuSideBar} from "./styled";
import {CSSTransition} from "react-transition-group";
import {inject, observer} from "mobx-react";
import {routes} from "../../../../routes.config";
import MailIcon from "../../../../svg/mail-with-circle.svg";
import VkIcon from "../../../../svg/vk-with-circle.svg";
import InstagramIcon from "../../../../svg/instagram-with-circle.svg";
import LogoIcon from "../../../../svg/logo.svg";
import ActiveLink from "../../../ActiveLink/ActiveLink";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import {CommunicationMethodList} from "../../../CommunicationMethodList/CommunicationMethodList";

export const MenuSideBar = inject('appStore')(observer(props => {
    const menuListRef = useRef(null);

    const {appStore} = props;
    const {throughData, isOpenMenu} = appStore;

    const handleClickMenuItem = (href) => {
        if (!href) {
            return
        }
        appStore.setIsOpenMenu(false);
    };

    const handlerClickBody = (e) => {
        if (!e.target.closest('.burger-button')) {
            if (!e.target.closest('.inner-menu-side-bar')) {
                appStore.setIsOpenMenu(false);
            }
        }
    };

    useEffect(() => {
        const $scrollableEl = menuListRef && menuListRef.current;

        if (isOpenMenu) {
            disablePageScroll($scrollableEl);
        } else {
            enablePageScroll($scrollableEl);
        }
    }, [isOpenMenu]);

    useEffect(() => {
        document.body.addEventListener('click', handlerClickBody);
        return () => {
            document.body.removeEventListener('click', handlerClickBody);
        }
    }, []);

    return (
        <StyledMenuSideBar>
            <CSSTransition
                in={isOpenMenu}
                timeout={1000}
                classNames="menu-side-bar"
                unmountOnExit>
                <div className="overlay">
                    <div className="inner inner-menu-side-bar">
                        <LogoIcon className="logo"/>
                        <a href={`tel:${throughData.phone}`} className="phone">{throughData.phone}</a>
                        <ul className='menu-list' ref={menuListRef}>
                            {routes.map(item => {
                                return (
                                    <li key={item.label} className='menu-list__item'
                                        onClick={() => handleClickMenuItem(item.href)}>
                                        <ActiveLink activeClassName="active-link" href={item.href} as={item.as}>
                                            <a className='link'>{item.label}</a>
                                        </ActiveLink>
                                        {item.subMenu &&
                                        <ul className='menu-list menu-list_nested'>
                                            {item.subMenu.map(item => {
                                                return (
                                                    <li key={item.label} className='menu-list__item'
                                                        onClick={() => handleClickMenuItem(item.href)}>
                                                        {!item.href
                                                            ? <a>{item.label}</a>
                                                            : <ActiveLink
                                                                activeClassName='active-link'
                                                                href={item.href}
                                                                as={item.as}>
                                                                <a className='link'>{item.label}</a>
                                                            </ActiveLink>}
                                                        {item.subMenu &&
                                                        <ul className='menu-list'>
                                                            {item.subMenu.map(item => {
                                                                return (
                                                                    <li key={item.label}
                                                                        className='menu-list__item'
                                                                        onClick={() => handleClickMenuItem(item.href)}>
                                                                        <ActiveLink activeClassName="active-link"
                                                                                    href={item.href} as={item.as}>
                                                                            <a className='link'>&mdash; {item.label}</a>
                                                                        </ActiveLink>
                                                                    </li>
                                                                )
                                                            })}
                                                        </ul>}
                                                    </li>
                                                )
                                            })}
                                        </ul>}
                                    </li>
                                )
                            })}
                        </ul>
                        <CommunicationMethodList className='communication-method-list'/>
                    </div>
                </div>

            </CSSTransition>
        </StyledMenuSideBar>
    )
}));

const socialListData = [
    {
        href: '/',
        icon: <MailIcon/>,
    },
    {
        href: '/',
        icon: <VkIcon/>,
    },
    {
        href: '/',
        icon: <InstagramIcon/>,
    }
];