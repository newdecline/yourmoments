import styled from "styled-components";

export const StyledMenuSideBar = styled('div')`
    .menu-side-bar {
        &-enter {
            background-color: rgba(0, 0, 0, 0);
            .inner {
                transform: translateX(-100%);
            }
        }
        &-enter-active {
            background-color: rgba(0, 0, 0, .6);
            transition: background-color 1s;
            .inner {
                transform: translateX(0);
                transition: transform 1s;
            }
        }
        &-exit {
            background-color: rgba(0, 0, 0, .6);
            transition: background-color 1s;
            .inner {
                transform: translateX(0);
            }  
        }
        &-exit-active {
            background-color: rgba(0, 0, 0, 0);
            transition: background-color 1s;
            .inner {
                transform: translateX(-100%);
                transition: transform 1s;
            }
        }
        &-enter-done {
            background-color: rgba(0, 0, 0, .6);
        }
    }
    .overlay {
        position: fixed;
        top: 0;
        left: 0;
        height: 100vh;
        width: 100%;
    }
    .inner {
        display: flex;
        padding: 11px 33px 24px 77px;
        flex-direction: column;
        height: 100%;
        background-color: #fff;
        box-sizing: border-box;
    }
    .logo {
        display: none;
    }
    .phone {
        margin-bottom: 30px;
        align-self: flex-end;
        border-bottom: ${({theme}) => `2px solid ${theme.minorColor}`};
        color: ${({theme}) => `${theme.minorColor}`};
        text-decoration: none;
        font-size: 26px;
        line-height: 38px;
        letter-spacing: 0.07em;
        font-weight: 700;
    }
    .communication-method-list {
      display: flex;
      margin: 30px 0;
      justify-content: flex-start;
    }
    .menu-list {
        margin: 0;
        padding: 0;
        list-style-type: none;
        font-size: 26px;
        line-height: 38px;
        font-weight: 700;
        box-sizing: border-box;
        a {
            text-decoration: none;
        }
        &_nested {
            padding-left: 20px;
            font-weight: 400;
            .menu-list {
                font-weight: 400;
            }
        }
    }
    .link {
        position: relative;
        &::after {
            content: '';
            position: absolute;
            bottom: 1px;
            left: 0;
            display: inline-block;
            width: 0;
            border-bottom: ${({theme}) => `2px solid ${theme.mainColor}`};
            transition: width .3s;    
        }
    }
    .active-link {
            color: ${({theme}) => theme.mainColor};
            &::after {
                width: 100%;
            }
        }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .inner {
            padding: 81px 33px 24px 77px;
            width: 287px;
        }
        .phone {
            display: none;
        }
        .logo {
            display: flex;
            align-self: center;
            justify-content: center;
            margin: 0 44px 50px 0;
            width: 120px;
            height: auto;
        }
        .menu-list {
            font-size: 20px;
            line-height: 30px;
            max-height: unset;
            overflow: unset;
        }
        .communication-method-list {
            display: none;
        }
        .link {
            &:hover {
                &::after {
                    width: 100%;
                }
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .inner {
            padding: 100px 33px 24px 77px;
            width: 310px;
        }
    }
`;