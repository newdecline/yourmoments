import React from 'react';
import Link from 'next/link';
import {observer, inject} from 'mobx-react';
import {StyledHeader} from "./styled";
import {MenuSideBar} from "./MenuSideBar/MenuSideBar";
import {CommunicationMethodList} from "../../CommunicationMethodList/CommunicationMethodList";

export const Header = inject('appStore')(observer(props => {
  const {appStore} = props;
  const {layoutData, isOpenMenu} = appStore;

  const handleClickBurgerButton = () => {
    appStore.setIsOpenMenu();
  };

  return (
    <>
      <StyledHeader isOpenMenu={isOpenMenu}>
        <div className="wrapper">
          <button
            className='burger-button'
            onClick={handleClickBurgerButton}>
            <span className="burger-button__item"/>
            <span className="burger-button__item"/>
            <span className="burger-button__item"/>
          </button>
          <a href={`tel:${layoutData.phone1 || ''}`} className="header-phone">{layoutData.phone1 || ''}</a>
          <Link href='/contacts#form'><a className='reserve-date'>Забронировать дату</a></Link>
          <CommunicationMethodList className='communication-method-list'/>
        </div>

        <MenuSideBar/>
      </StyledHeader>
    </>
  )
}));

