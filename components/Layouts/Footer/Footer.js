import React from 'react';
import {observer, inject} from 'mobx-react';
import {StyledFooter} from "./styled";
import {routes} from "../../../routes.config";
import Link from "next/link";
import {CommunicationMethodList} from "../../CommunicationMethodList/CommunicationMethodList";

export const Footer = inject('appStore')(observer(props => {
  const {
    appStore: {
      layoutData,
    }
  } = props;

  return (
    <StyledFooter>
      <div className="grid-container">
        <div className="grid-item contact-us">
          {layoutData.phone1 && <a href={`tel:${layoutData.phone1}`} className="contact-us__tel">
            Свяжитесь с нами:
            <span className="phone-number">{layoutData.phone1}</span>
          </a>}
          <CommunicationMethodList className='communication-method-list'/>
        </div>

        <ul className="grid-item navigation-list">
          {routes.map(item => (
            <li key={item.label} className="navigation-list__item">
              <Link href={item.href} as={item.as}><a>{item.label}</a></Link>
            </li>
          ))}
        </ul>

        <div className='grid-item developed-by'>
          <a
            href='https://vk.com/aleksandra.strahova'
            target='_blank'
            rel='noreferrer noopener'>
            <span className="developed-by__title">Сайт разработан:</span>
            <span className="developed-by__name">StrahovaAleksandra</span>
          </a>
        </div>
      </div>

      <div className="copyright"><span>©</span>&nbsp;Все права защищены {new Date().getFullYear()}</div>
    </StyledFooter>
  )
}));

