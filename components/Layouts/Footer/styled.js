import styled from "styled-components";

export const StyledFooter = styled('footer')`
    background-color: ${({theme}) => theme.mainColor};
    .container {
        max-width: unset;
    }
    .grid-container {
        padding: 16px 31px;
        display: grid;
        grid-template-columns: 100%;
        grid-template-rows: auto;
        grid-template-areas: 'navigation-list'
                              'contact-us'
                              'developed-by';
        color: #fff;
        box-sizing: border-box;
    }
    .copyright {
        padding: 20px;
        display: flex;
        align-items: center;
        justify-content: center;
        font-size: 18px;
        line-height: 27px;
        box-sizing: border-box;
        background-color: #fff;
        span {
            display: block;
            margin-top: -5px;
        }
    }
    .contact-us {
        grid-area: contact-us;
        &__tel {
            display: none;
        }
    }
    .communication-method-list {
      margin: 0 0 20px 0;
      .item {
        margin-right: 30px;
        &:last-of-type {
          margin-right: 0;
        }
        a {
          display: flex;
          align-items: center;
          justify-content: center;
        }
        svg {
          width: 55px;
          height: auto;
        }
        circle {
          fill: #fff;
        }
        path {
          fill: ${({theme}) => theme.mainColor} 
        }
      }
    }
    .navigation-list {
        margin: 0 0 9px 0;
        grid-area: navigation-list;
        display: grid;
        grid-template-columns: 1fr auto;
        grid-template-rows: auto;
        padding: 0;
        list-style-type: none;
        font-size: 20px;
        line-height: 29px;
         &__item {
            a {
                color: #fff;
                text-decoration: none;
            }
         }
    }
    .developed-by {
        display: flex;
        justify-content: center;
        grid-area: developed-by;
        font-size: 18px;
        line-height: 27px;
        a {
            color: #fff;
            text-decoration: none;
        }
        &__title {
            padding-right: 5px;
        }
        &__name {
            font-family: 'Circe-Bold';
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .grid-container {
            padding: 30px;
            display: grid;
            grid-template-columns: 165px auto 162px;
            grid-template-rows: auto;
            grid-template-areas: 'contact-us navigation-list developed-by';
            justify-content: space-between;
        }
        .contact-us {
            grid-area: contact-us;
            &__tel {
                display: inline-block;
                font-size: 20px;
                line-height: 29px;
                text-decoration: none;
                color: #fff;
            }
        }
        .phone-number {
            margin-bottom: 5px;
            display: block;
            font-family: 'Circe-Bold', sans-serif;
            letter-spacing: 1.5px;
        }
        .communication-method-list {
            justify-content: flex-start;
            margin: 0;
            .item {
                margin-right: 9px;
                svg {
                    width: 26px;
                }
                circle, path {
                    transition: fill .3s;
                }
                &:hover {
                    circle {
                        fill: ${({theme}) => `${theme.minorColor}`}
                    }
                    path {
                        fill: #fff;
                    }
                }
            }
        }
        .developed-by {
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-end;
            a {
                display: flex;
                flex-direction: column;
            }
        }
        .navigation-list {
            grid-template-columns: repeat(3, auto);
            grid-gap: 0 30px;
            align-self: flex-start;
            a {
                position: relative;
                &::after {
                    content: '';
                    position: absolute;
                    bottom: 0;
                    left: 0;
                    width: 0;
                    display: block;
                    height: 2px;
                    background-color: #fff;
                    transition: width .3s;
                }
                &:hover {
                    &::after {
                        width: 100%;
                    }
                }
            }
        }
        .copyright {
            padding: 13px 30px;
            justify-content: flex-start;
            font-size: 13px;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        // .container {
        //     max-width: ${({theme}) => `${theme.breakPoints.desktop.containerWidth}px`};
        // }
        // .grid-container {
        //     padding: 30px 0;
        // }
    }
`;