import React from "react";
import PropTypes from "prop-types";
import {Header} from "./Header/Header";
import {Footer} from "./Footer/Footer";
import Head from "next/head";

export const MainLayout = props => {
  const {
    children,
  } = props;

  return (
    <>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
        <link rel="icon" type="image/x-icon" href="/assets/favicon.svg"/>
        <title>Фотограф | Видеограф</title>
      </Head>

      <Header/>
      {children}
      <Footer/>
    </>
  )
};

MainLayout.propTypes = {
  children: PropTypes.object
};