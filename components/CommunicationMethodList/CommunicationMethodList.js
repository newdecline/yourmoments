import React from 'react';
import {observer, inject} from 'mobx-react';
import {StyledCommunicationMethodList} from "./styled";
import MailIcon from "../../svg/mail-with-circle.svg";
import VkIcon from "../../svg/vk-with-circle.svg";
import InstagramIcon from "../../svg/instagram-with-circle.svg";

export const CommunicationMethodList = inject('appStore')(observer(props => {
  const {
    appStore: {
      layoutData
    },
    className
  } = props;

  const hasCommunicationMethodList = layoutData.email && layoutData.vkontakte && layoutData.instagram;

  if (!hasCommunicationMethodList) {
    return null;
  }

  return <StyledCommunicationMethodList className={className}>
    {layoutData.email && <li className="item">
      <a
        href={`mailto:${layoutData.email}`}
        className='link'
        target='_blank'
        rel='noopener noreferrer'><MailIcon/></a>
    </li>}
    {layoutData.vkontakte && <li className="item">
      <a
        href={layoutData.vkontakte}
        className='link'
        target='_blank'
        rel='noopener noreferrer'><VkIcon/></a>
    </li>}
    {layoutData.instagram && <li className="item">
      <a
        href={layoutData.instagram}
        className='link'
        target='_blank'
        rel='noopener noreferrer'><InstagramIcon/></a>
    </li>}
  </StyledCommunicationMethodList>
}));