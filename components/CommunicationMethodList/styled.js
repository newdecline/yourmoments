import styled from 'styled-components';

export const StyledCommunicationMethodList = styled('ul')`
  padding: 0;
  margin: 0 0 34px 0;
  display: flex;
  justify-content: center;
  list-style-type: none;
  .item {
    margin-right: 30px;
    &:last-child {
      margin-right: 0;
    }
  }
  .link {
    display: flex;
    circle {
      fill: ${({theme}) => theme.minorColor}
    }
    path {
      fill: #fff;
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    .link {
      display: flex;
      &:hover {
        cursor: pointer;
        path {
          fill: ${({theme}) => `${theme.mainColor}`};
        }
      }
    }
  }
`;