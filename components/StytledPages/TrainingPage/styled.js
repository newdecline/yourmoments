import styled from "styled-components";

export const Styled = styled('div')`
  padding-top: 67px;
  .container {
    padding: 0 12px;
    box-sizing: border-box;
  }
  .top-banner {
    margin-bottom: 28px;
    height: 443px;
    background-image: ${({bannerBg}) => `url(${bannerBg})`};
    background-repeat: no-repeat;
    background-position: center -73px;
    background-size: 750px;
  }
  .page-header {
    &__title {
      margin-bottom: 12px;
      font-family: 'Circe-Light', sans-serif;
      font-size: 24px;
      line-height: 28px;
      text-align: center;
      text-transform: uppercase;
    }
    &__subtitle > p {
      margin-top: 0;
      margin-bottom: 12px;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      white-space: pre-wrap;
    }
    &__subtitle-2 > p {
      margin-top: 0;
      margin-bottom: 21px;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      text-transform: uppercase;
    }
  }
  .card-wrap {
    margin-bottom: 14px;
    display: grid;
    grid-gap: 16px 0;
  }
  .cost {
    margin-bottom: 13px;
    font-size: 20px;
    line-height: 29px;
    text-align: center;
    text-transform: uppercase;
    &__divider {
      display: none;
    }
    &__value {
      display: block;
      font-family: 'Circe-Bold', sans-serif;
      font-size: 40px;
      line-height: 59px;
      color: ${({theme}) => theme.mainColor};
    }
    &__currency {
      font-family: 'Circe-Regular', sans-serif;
      font-size: 20px;
      line-height: 29px;
    }
  }
  .contact-us {
    &__title {
      margin-bottom: 15px;
      font-size: 18px;
      line-height: 27px;
      text-align: center;
    }
  }
  .social-list {
    padding: 0;
    margin: 0;
    display: flex;
    justify-content: center;
    list-style-type: none;
    &__item {
      margin-right: 30px;
      &:last-child {
        margin-right: 0;
      }
    }
  }
  .social-link {
    display: flex;
    circle {
      fill: ${({theme}) => theme.minorColor}
    }
    path {
      fill: #fff;
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    padding-top: 81px;
    .container {
      padding: 0;
    }
    .top-banner {
      margin-bottom: 36px;
      background-position: center -133px;
      background-size: 1371px;
    }
    .page-header {
      &__title {
        margin-bottom: 16px;
      }
      &__title-inner {
        position: relative;
        &::after, 
        &::before {
          content: '';
          position: absolute;
          top: calc(50% - 3px);
          transform: translateY(-50%);
          width: 98px;
          height: 1px;
          background-color: ${({theme}) => theme.minorColor};
        }
        &::after {
          left: calc(100% + 13px);
        }
        &::before {
          right: calc(100% + 13px);
        }
      }
      &__subtitle-2 > p {
        margin-bottom: 27px;
      }
    }  
    .card-wrap {
      grid-template-columns: 1fr 1fr;
      grid-template-rows: auto;
      grid-gap: 20px;
    }
    .cost {
      font-size: 24px;
      line-height: 35px;
      &__divider {
        display: inline;
      }
      &__value {
        display: inline-block;
      }
      &__currency {
        font-size: 24px;
        line-height: 35px;
      }
    }
    .social-link {
      &:hover {
        cursor: pointer;
        svg circle {
          fill: ${({theme}) => theme.mainColor}
        }
      }
      svg {
        width: 40px;
        height: auto;
      }
      svg circle {
        transition: fill .3s;
      }
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    .container {
      padding: 0 30px; 
    }
    .top-banner {
      margin-bottom: 45px;
      background-position: center -133px;
      background-size: 1371px;
    }
    .card-wrap {
      grid-gap: 30px;
      margin-bottom: 26px;
    }
    .description-list {
      &__item {
        font-size: 18px;
        line-height: 27px;
      }
    }
  }
  @media (min-width: 1367px) {
    .top-banner {
      background-position: center -170px;
      background-size: 100%;
    }
  }
  @media (min-width: 1920px) {
    .top-banner {
      background-position: center -213px;
      background-size: 100%;
    }
  }
`;

export const StyledModuleCard = styled('div')`
  padding: 44px 13px 44px 13px;
  display: flex;
  flex-direction: column;
  border: 3px solid #EACDC4;
  text-align: center;
  box-sizing: border-box;
  .number-module {
    margin-bottom: 5px;
    font-family: 'Circe-Bold', sans-serif;
    font-size: 24px;
    line-height: 35px;
    text-transform: uppercase;
  }
  .lesson-title {
    margin-bottom: 16px;
    padding-bottom: 7px;
    position: relative;
    font-size: 18px;
    line-height: 27px;
    text-align: center;
    white-space: pre-wrap;
    &::after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 50%;
      transform: translateX(-50%);
      display: inline-block;
      width: 146px;
      border-bottom: ${({theme}) => `2px solid ${theme.minorColor}`}
    }
  }
  .lesson-subtitle {
    margin-bottom: 10px;
  }
  .description-list ul {
    margin: 0;
    padding: 0;
    list-style-type: none;
    li {
      position: relative;
      padding: 19px 0;
      &::after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 50%;
        transform: translateX(-50%);
        display: inline-block;
        width: 90%;
        height: 2px;
        background-color: ${({theme}) => theme.mainColor};
      }
      &:last-child {
        padding: 14px 0 0 0;
        &::after {
          display: none;
        }
      }
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    .description-list ul li {
      font-size: 18px;
      line-height: 27px;
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    padding: 45px 78px;
    .lesson-title {
      font-size: 20px;
      line-height: 29px;
    }
    .lesson-subtitle {
      font-size: 18px;
      line-height: 27px;
    }
  }
`;