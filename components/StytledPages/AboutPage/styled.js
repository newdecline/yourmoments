import styled from 'styled-components';

export const StyledPage = styled('div')`
    padding-top: 67px;
    .title {
        width: 317px;
        margin: 25px auto;
        text-align: center;
        font-size: 26px;
        line-height: 38px;
    }
    .paragraph-wrap {
        order: 3;
        margin: 26px 33px;
    }
    .paragraph {
        font-size: 18px;
        line-height: 27px;
        &:first-child {
            margin-top: 0; 
        }
        &:last-child {
            margin-bottom: 0; 
        }
        &_desktop-visible {
            display: none;
        }
    }
    .paragraph-text-wrap {
        display: inline;
        margin: 0;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .container {
            display: flex;
            flex-direction: column;
        }
        .paragraph-wrap {
            display: grid;
            grid-auto-flow: column;
            grid-template-columns: 382px 295px;
            grid-template-rows: repeat(2, auto);
            grid-gap: 33px;
            order: 3;
            margin: 0;
        }
        .title {
            order: 2;
            width: 341px;
            margin: 33px auto 25px 0;
            text-align: left;
            font-size: 28px;
            line-height: 41px;
        }
        .paragraph {
            margin: 0;
            &:last-child {
                margin-top: -60px;
            }
            &_desktop-visible {
                display: block;
            }
        }
        .paragraph-text-wrap {
            display: none;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .title {
            width: 363px;
            font-size: 30px;
        }
        .paragraph-wrap {
            grid-template-columns: 365px 329px 304px;
            grid-template-rows: repeat(1, auto);
            grid-gap: 71px;
        }
        .paragraph {
            &:nth-child(2) {
                margin-top: -55px;
            }
            &:last-child {
                margin-top: -55px;
            }
            &_desktop-visible {
                display: none;
            }
        }
        .paragraph-text-wrap {
            display: inline;
        }
    }
`;

export const StyledSlider = styled('div')`
    position: relative;
    .swiper-button-prev,
    .swiper-button-next {
        background-color: transparent;
        &::after {
            content: '';
        }
        &:disabled {
            opacity: .4;
        }
    }
    .swiper-button-prev {
        transform: rotate(180deg);
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        order: 1;
        .swiper-button-prev,
        .swiper-button-next {
            display: none;
        }
        .swiper-wrapper {
            justify-content: space-between;
        }
        .my-slide-1 {
            width: 441px;
        }
        .my-slide-2 {
            width: 269px;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .my-slide-1 {
            width: 718px;
        }
        .my-slide-2 {
            width: 401px;
        }
    }
`;

export const StyledSlide = styled('div')`
    .img-wrap {
        position: relative;
        width: 100%;
        padding-top: 118%;
        background-repeat: no-repeat;
    }
    .img-wrap-1 {
        background-size: 199%;
        background-position: -208px -23px;
    }
    .img-wrap-2 {
        background-size: 120%;
        background-position: -46px -229px;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .img-wrap-1 {
            padding-top: 87%;
            background-size: 124%;
            background-position: -31px -15px;
        }
         .img-wrap-2 {
            padding-top: 142.9%;
            background-size: 140%;
            background-position: -37px -179px;
         }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .img-wrap-1 {
            padding-top: 61.7%;
            background-size: 100%;
            background-position-x: 0;
        }
         .img-wrap-2 {
            padding-top: 110.5%;
            background-size: 112%;
            background-position: -37px -225px;
         }
    }
`;