import styled from 'styled-components';

export const StyledPage = styled('div')`
    padding-top: 67px;
    .links-wrap {
        display: flex;
        flex-direction: column;
        &__item {
            &:nth-child(1) {
                svg {
                    
                }
            }
            &:nth-child(2) {
                svg {
                    transform: translate(0, 4px);
                }
            }
            &:nth-child(3) {
                svg {
                    transform: translate(0, -6px);
                }
            }
        }
    }
    .link {
        display: block;
        position: relative;
        overflow: hidden;
        padding-top: 117.6%;
        img {
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            object-fit: cover;
            will-change: transform;
            transform-origin: center;
            transform: scale(1);
            transition: transform 1s;
        }
        &:hover {
            img {
                transform: scale(1.05);
            }
        }
    }
    .category-name-wrap {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 97px;
        overflow: hidden;
    }
    .category-name {
        display: flex;
        align-items: center;
        height: 100%;
        position: relative;
        &::after {
            content: '';
            position: absolute;
            bottom: 0;
            left: 0;
            width: 0;
            height: 1px;
            background-color: ${({theme}) => theme.minorColor};
            transition: width 1s;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding-top: 81px;
        .links-wrap {
            flex-direction: row;
            justify-content: space-between;
            margin-bottom: 15px;
            &__item {
                width: calc(33.33% - 6px);
                &:hover {
                    .category-name::after {
                        width: 100%;
                    }
                }
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        padding-top: 149px;
        .links-wrap {
            margin-bottom: 25px;
        }
    }
`;

export const StyledSlider = styled('div')`
    position: relative;
    .swiper-wrapper {
        padding-bottom: 22px;
    }
    .swiper-button-prev,
    .swiper-button-next {
        background-color: transparent;
        display: flex;
        &::after {
            content: '';
        }
        &:disabled {
            opacity: .4;
        }
    }
    .swiper-button-prev {
        transform: rotate(180deg);
    }
    .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
        margin: 0 9px;
    }
    .swiper-pagination-bullet {
        width: 6px;
        height: 6px;
        opacity: 1;
        background-color: ${({theme}) => theme.minorColor};
    }
    .swiper-pagination-bullet-active {
        background-color: ${({theme}) => theme.mainColor};
    }
    .couple-name {
        margin: 12px 0;
        text-align: center;
        font-size: 24px;
        line-height: 35px;
    }
    &.single-slide {
        .swiper-wrapper {
            padding-bottom: 0;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .swiper-container {
            width: 586px;
            margin: 0 auto;
        }
        .couple-name {
            text-align:left;
        }
        .swiper-button-prev,
        .swiper-button-next {
            svg path {
                fill: ${({theme}) => theme.minorColor}}
            }
        .swiper-slide {
            min-height: 388px;
        }
        &.single-slide {
            .swiper-slide {
                min-height: 388px;
            }
        }
    }   
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .swiper-container {
            width: 740px;
        }
        .swiper-button-prev {
            left: 70px;
        }
        .swiper-button-next {
            right: 70px;
        }
        .swiper-slide {
            min-height: 475px;
        }
        &.single-slide {
            .swiper-slide {
                min-height: 475px;
            }
        }
    }
`;