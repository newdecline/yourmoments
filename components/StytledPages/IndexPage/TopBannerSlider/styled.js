import styled from "styled-components";

export const StyledTopBannerSlider = styled('div')`
    position: relative;
    .swiper-wrapper {
        padding-bottom: 32px;
    }
    .swiper-button-next, .swiper-button-prev {
        display: flex;
        align-items: center;
        justify-content: center;
        width: 55px;
        height: 55px;
        background: rgba(255, 255, 255, 0.7);
        border-radius: 50%;
        top: calc(50% - 26px);
        &::after {
            content: '';
        }
        path {
            fill: ${({theme}) => `${theme.minorColor}`}
        }
        &:disabled {
            opacity: .3;
        }
    }
    .swiper-button-next {
        
    }
    .swiper-button-prev {
        transform: rotate(180deg);
    }
    .swiper-pagination {
        display: flex;
        justify-content: center;
        bottom: 0!important;
    }
    .swiper-pagination-bullet {
        background-color: ${({theme}) => `${theme.minorColor}`};
        width: 6px;
        height: 6px;
        opacity: 1;
    }
    .swiper-pagination-bullet-active {
        background-color: ${({theme}) => `${theme.mainColor}`};
    }
    .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
        margin: 0 9px;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .swiper-button-next, .swiper-button-prev {
            background-color: transparent;
            path {
                fill: #fff;
            }
        }
        .swiper-button-next {
            right: 15%;
        } 
        .swiper-button-prev {
            left: 15%;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        
    }
`;

export const StyledSlide = styled('div')`
    box-sizing: border-box;
    .img-wrap {
        position: relative;
        overflow: hidden;
        padding-top: 55%;
        img {
            position: absolute;
            height: 100%;
            width: 100%;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            object-fit: cover;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .img-wrap {
            padding-top: 40%;
        }
    }
`;