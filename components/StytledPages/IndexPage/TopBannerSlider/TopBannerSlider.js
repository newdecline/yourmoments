import React, {useState, useEffect} from 'react';
import {inject, observer} from "mobx-react";
import Swiper from "react-id-swiper";
import {StyledSlide, StyledTopBannerSlider} from "./styled";
import ArrowIcon from "../../../../svg/arrow.svg";

export const TopBannerSlider = inject('appStore')(observer(props => {
  const {banner} = props;

  const [swiper, setSwiper] = useState(null);
  const [reachBeginning, setReachBeginning] = useState(true);
  const [reachEnd, setReachEnd] = useState(false);

  useEffect(() => {
    if (swiper !== null) {
      swiper.on('slideChange', () => {
        setReachBeginning(swiper.isBeginning);
        setReachEnd(swiper.isEnd);
      });
    }
  }, [swiper]);

  const settingsSlider = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
      dynamicBullets: true,
      dynamicMainBullets: 5,
    },
    getSwiper: setSwiper
  };

  const goNext = () => {
    if (swiper !== null) {
      swiper.slideNext();
    }
  };

  const goPrev = () => {
    if (swiper !== null) {
      swiper.slidePrev();
    }
  };

  return (
    <StyledTopBannerSlider>
      <Swiper {...settingsSlider}>
        {
          banner.map((item) => (
            <StyledSlide key={item.slideId}>
              <div className="img-wrap">
                <img src={item.croppedImage.path} alt="alt"/>
              </div>
            </StyledSlide>
          ))
        }
      </Swiper>
      <button
        disabled={reachBeginning}
        className="swiper-button-prev"
        onClick={goPrev}><ArrowIcon/></button>
      <button
        disabled={reachEnd}
        className="swiper-button-next"
        onClick={goNext}><ArrowIcon/></button>
    </StyledTopBannerSlider>
  )
}));