import styled from "styled-components";

export const StyledPage = styled('div')`
  padding-top: 67px;
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    padding-top: 81px;
  }
`;

export const StyledWatchUsOnInstagram = styled('div')`
  text-align: center;
  .preloader {
    margin: 20px 0;
    font-size: 20px;
    line-height: 29px;
    font-weight: 500;
  }
  .title {
    margin: 20px 20px 0 20px;
    font-size: 20px;
    line-height: 29px;
    font-weight: 700;
  }
  .link {
    margin-bottom: 14px;
    text-decoration: none;
    font-size: 20px;
    line-height: 29px;
  }
  .instagram {
    margin-top: 20px;
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr 1fr;
    grid-gap: 8px;
  }
  .instagram__link {
    position: relative;
    padding-top: 100%;
    &:nth-child(n+5) {
      display: none;
    }
    img {
      position: absolute;
      top: 0;
      left: 0;
      margin: 0!important;
      width: 100%!important;
      height: 100%;
      object-fit: cover;
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    .instagram {
      grid-template-columns: repeat(4, 1fr);
      .instagram__link {
        box-sizing: border-box;
        overflow: hidden;
        &:nth-child(n+5) {
          display: block;
        }
        &:hover {
          img {
            transform: scale(1.05);
          }
        }
        img {
          will-change: transform;
          transform-origin: center;
          transform: scale(1);
          transition: transform 1s;
        }
      }
    }
    .link {
      position: relative;
      &::after {
        content: '';
        position: absolute;
        bottom: 0;
        left: 0;
        width: 0;
        display: block;
        height: 2px;
        background-color: ${({theme}) => `${theme.mainColor}`};
        transition: width .3s;
      }
      &:hover {
        &::after {
          width: 100%;
        }
      }
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    .instagram {
      grid-template-rows: unset;
      .instagram__link {
        padding-top: 99%;
        &:last-child {
          display: block;
        }
      }
    }
  }
`;