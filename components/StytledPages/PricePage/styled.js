import styled from 'styled-components';

export const Styled = styled('div')`
  padding-top: 67px;
  .page-title {
    margin: 26px 0;
    text-align: center;
    font-size: 24px;
    line-height: 35px;
    text-transform: uppercase;
    font-family: 'Circe-Light';
    font-weight: 300;
  }
  .page-note {
    margin: 0 18px;
    font-size: 18px;
    line-height: 27px;
    text-align: center;
  }
  .card-wrap {
    margin-bottom: 14px;
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
   padding-top: 81px;
   .page-title {
      margin: 40px 0 26px 0;
   }
    .card-wrap {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      margin: 0 -11px;
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    .card-wrap {
      margin-bottom: 32px;
    }
    .page-note {
      font-size: 22px;
      line-height: 32px;
    }
    }
`;

export const StyledTariffCard = styled('div')`
  margin: 0 13px;
  .inner {
    padding: 26px 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    border: ${({theme}) => `3px solid ${theme.mainColor}`};
    box-sizing: border-box;
  }
  .duration {
    margin-bottom: 15px;
    text-align: center;
    font-size: 22px;
    line-height: 32px;
    font-family: 'Circe-Light', sans-serif;
    font-weight: 300;
  }
  .tariff {
    margin-bottom: 28px;
    font-size: 40px;
    line-height: 49px;
    font-family: 'Circe-Regular', sans-serif;
    border-bottom: ${({theme}) => `2px solid ${theme.minorColor}`};
  }
  .name-service {
    text-transform: uppercase;
    font-size: 30px;
    line-height: 44px;
    font-family: 'Circe-Light', sans-serif;
  }
  .cost {
    margin-bottom: 6px;
    font-family: 'Circe-Bold', sans-serif;
    font-size: 30px;
    line-height: 44px;
  }
  .results-list > ul {
    margin-bottom: 30px;
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 0;
    list-style-type: none;
    li {
      font-size: 16px;
      line-height: 24px;
      white-space: pre-wrap;
      text-align: center;
    }
  }
  .reserve-date {
    display: inline-block;
    padding: 9px 43px;
    font-size: 18px;
    line-height: 27px;
    text-decoration: none;
    background: ${({theme}) => theme.mainColor};
    color: #fff;
    border-radius: 5px;
  }
  .title-surcharge {
    font-size: 18px;
    line-height: 27px;
    font-family: 'Circe-Bold';
  }
  .surcharge-list > ul {
    padding: 0;
    margin-bottom: 60px;
    list-style-type: none;
    li {
      font-size: 16px;
      line-height: 24px;
      text-align: center;
    }
  }
  .card-notes > ul {
    margin: 10px 0 20px 0;
    padding: 0;
    list-style-type: none;
    li {
      font-size: 14px;
      line-height: 21px;
      text-align: center;
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    margin: 0 11px;
    width: calc(50% - 22px);
    .title-surcharge {
      margin-top: auto;
    }
    .card-notes > ul {
      margin: 17px 0 20px 0;
    }
    .reserve-date {
      margin-top: auto;
      opacity: .8;
      transition: opacity .3s;
      &:hover {
        cursor: pointer;
        opacity: 1;
      }
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    width: calc(33.33% - 22px);
    .inner {
      padding: 26px 0;
    }
    .duration {
      margin-bottom: 37px;
    }
    .tariff {
      margin-bottom: 49px;
    }
    .title-surcharge {
      margin-top: 0;
    }
    .card-notes > ul {
      margin: 25px 0 42px 0;
    }
    .name-service {
      line-height: 40px;
    }
    .cost {
      margin-bottom: 0;
    }
    .results-list > ul {
      margin: 0;
      padding: 0 0 25px 0;
      box-sizing: border-box;
    }
    .photo-results-list > ul {
      padding: 0 0 10px 0;    
    }
  }
`;