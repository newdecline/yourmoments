import React from 'react';
import PropTypes from 'prop-types';
import {useRouter} from 'next/router';
import Link from 'next/link';
import {StyledBreadCrumbs} from "./styled";

export const BreadCrumbs = props => {
    const {root, title} = props;

    const router = useRouter();
    const {query: {category}} = router;

    let nameCategory = '';

    switch (category) {
        case 'paired':
            nameCategory = 'Парные';
            break;
        case 'wedding':
            nameCategory = 'Свадебные';
            break;
        case 'business-content':
            nameCategory = 'Контент для бизнеса';
            break;
        default: nameCategory = null
    }

    return <StyledBreadCrumbs>
        <Link href={root.href}>
            <a className='link'>{root.label}</a>
        </Link>
        <Link href={`${root.href}/[category]`} as={`${root.href}/${category}`}>
            <a className='link'>{nameCategory}</a>
        </Link>
        {title && <a className='link'>{title}</a>}
    </StyledBreadCrumbs>
};

BreadCrumbs.propTypes = {
    root: PropTypes.object,
    title: PropTypes.string
};