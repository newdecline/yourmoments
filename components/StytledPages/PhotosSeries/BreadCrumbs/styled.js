import styled from 'styled-components';

export const StyledBreadCrumbs = styled('div')`
    margin: 23px 14px;
    .link { 
        position: relative;
        text-decoration: none;
        margin-right: 41px;
        font-weight: 300;
        font-size: 16px;
        line-height: 24px;
        font-family: 'Circe-Light';
        white-space: nowrap;
        &:not(:last-child)::after {
            content: url('/assets/svg/long-arrow.svg');
            display: flex;
            position: absolute;
            top: -1px;
            right: -33px;
        }
        &:last-child {
            margin-right: 0;
            pointer-events: none;
            &::before {
                content: '';
                position: absolute;
                bottom: 0;
                left: 0;
                display: block;
                background-color: ${({theme}) => theme.minorColor};
                height: 1px;
                width: 100%;
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
         margin: 30px 0;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
         margin: 45px 0;
    }
`;