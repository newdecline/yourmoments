import styled from "styled-components";

export const StyledPage = styled('div')`
    padding-top: 67px;
    color: ${({theme}) => theme.minorColor};
    .grid-container {
        display: grid;
    }
    .card {
        flex-direction: column;
        text-decoration: none;
        display: flex;
        &__img-wrap {
            position: relative;
            overflow: hidden;
            padding-top: 62%;
        }
        &__img {
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
            will-change: transform;
            transition: transform 1s;
        }
        &__name {
            margin: 13px 0 18px 0;
            align-self: center;
            position: relative;
            font-size: 18px;
            line-height: 27px;
            white-space: pre-wrap;
            &::after,
            &::before {
                content: '';
                position: absolute;
                display: block;
                top: 13px;
                width: 40px;
                height: 1px;
                background-color: ${({theme}) => theme.minorColor};
                
            }
            &::after {
                right: calc(100% + 7px);
            }
            &::before {
                left: calc(100% + 7px);
            }
        }
    }
    .button-more {
        margin: 0 auto;
        padding: 8px 40px;
        display: flex;
        align-items: center;
        border: 1px solid ${({theme}) => theme.minorColor};
        border-radius: 5px;
        background-color: transparent;
        box-sizing: border-box;
        transition: background-color .3s, border .3s;
        &__text {
            font-size: 18px;
            line-height: 27px;
            color: ${({theme}) => theme.minorColor};
            transition: color .3s;
        }
        &__svg {
            margin-left: 4px;
        }
        path {
            transition: fill .3s
        }
    }
    .no-series {
      text-align: center;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding-top: 81px;
        .grid-container {
            grid-template-columns: repeat(2, 1fr);
            grid-template-rows: auto;
            grid-gap: 0 20px;
        }
        .card {
            &:nth-child(5),
            &:nth-child(6) {
                display: flex;
            }
            &__name {
                &::after {
                    display: none;
                }
                &::before {
                    left: 0;
                    top: unset;
                    bottom: 0;
                    width: 0;
                    transition: width 1s;
                }
            }
            &:hover {
                .card__name::before {
                    width: 100%;
                }
                .card__img {
                    transform: scale(1.04);
                }
            }
        }
        .button-more {
            &:hover {
                border-color: ${({theme}) => theme.mainColor};
                background-color: ${({theme}) => theme.mainColor};
                .button-more__text {
                    color: #fff;
                }
                path {
                    fill: #fff;
                }
            }
        }
        .no-series {
        text-align: left;
      }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .grid-container {
            grid-template-columns: repeat(3, 1fr);
            grid-template-rows: auto;
            grid-gap: 0 20px;
        }
        .card {
            &:nth-child(7),
            &:nth-child(8),
            &:nth-child(9) {
                display: flex;
            }
        }
    }
`;
