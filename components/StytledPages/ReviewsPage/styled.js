import styled from "styled-components";

export const StyledPage = styled('div')`
    padding-top: 67px;
    .page-title {
        margin: 24px 0;
        text-transform: uppercase;
        font-size: 24px;
        line-height: 35px;
        text-align: center;
        font-family: 'Circe-Light';
        font-weight: 300;
    }
    .slider-wrap {
        position: relative;
    }
    .swiper-button-next, .swiper-button-prev {
        top: calc(50% - 205px);
        display: flex;
        align-items: center;
        justify-content: center;
        width: 55px;
        height: 55px;
        background: rgba(255, 255, 255, 0.7);
        border-radius: 50%;
        &::after {
            content: '';
        }
        path {
            fill: ${({theme}) => `${theme.minorColor}`}
        }
        &:disabled {
            opacity: .3;
        }
    }
    .swiper-button-next {
        
    }
    .swiper-button-prev {
        transform: rotate(180deg);
    }
    .swiper-pagination {
        display: flex;
        justify-content: center;
        bottom: 0!important;
    }
    .swiper-pagination-bullet {
        background-color: ${({theme}) => `${theme.minorColor}`};
        width: 6px;
        height: 6px;
        opacity: 1;
    }
    .swiper-pagination-bullet-active {
        background-color: ${({theme}) => `${theme.mainColor}`};
    }
    .swiper-container-horizontal > .swiper-pagination-bullets .swiper-pagination-bullet {
        margin: 0 9px;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding-top: 81px;
        .page-title {
            margin: 32px 0;
        }
        .slider-wrap {
            margin: 0 auto;
        }
        .swiper-button-next, .swiper-button-prev {
            top: calc(50% - 36px);
            background-color: transparent;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .slider-wrap {
            width: 823px;
            margin: 0 auto;
        }
    }
`;

export const StyledSlide = styled('div')`
    margin: 0 0 34px 0;
    height: 100%;
    display: flex;
    flex-direction: column;
    .inner-slide {
        display: flex;
        flex-direction: column;
    }
    .img-wrap {
        position: relative;
        padding-top: 95%;
        img {
            position: absolute;
            object-fit: cover;
            height: 100%;
            width: 100%;
            top: 0;
            left: 0;
        }
    }
    .wrapper {
        display: flex;
        height: 100%;
        flex-direction: column;
        justify-content: space-between;
        padding: 27px 32px 23px 32px;
        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.13);
    }
    .reviews-text {
        font-size: 18px;
        line-height: 25px;
        text-align: center;
        p {
            margin: 0 0 10px 0;
        }
    }
    .reviews-text-inner {
        white-space: pre-wrap;
        display: inline-block;
        margin-bottom: 10px;
    }
    .scrollbars {
        margin-bottom: 20px;
    }
    .reviews-author {
        font-size: 22px;
        line-height: 32px;
        font-family: 'Circe-Bold';
        text-align: center;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding: 0 45px;
        box-sizing: border-box;
        .inner-slide {
            margin: 0 15px;
            flex-direction: row;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.13);
        }
        .img-wrap {
            width: 248px;
            margin-right: 31px;
            padding-top: 59%;
        }
        .wrapper {
            padding: 65px 14px 52px 0;
            flex: 1;
            height: auto;
            box-shadow: none;
            box-sizing: border-box;
        }
        .reviews-text {
            text-align: left;
            p {
                padding-right: 35px;
            }
        }
        .reviews-author {
            text-align: left;
        }
        .track-vertical {
            right: 0;
            height: 99%;
            border: 1px solid #e5e5e5;
            border-radius: 6px;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        margin: 0 0 43px 0;
        .wrapper {
            padding: 70px 14px 48px 0;
        }
        .img-wrap {
            margin-right: 76px;
        }
        .reviews-text {
            p {
                padding-right: 54px;
            }
        }
        .img-wrap {
            padding-top: 44.9%;
        }
    }
`;