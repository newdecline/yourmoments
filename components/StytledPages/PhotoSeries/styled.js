import styled from 'styled-components';

export const StyledPage = styled('div')`
    padding-top: 67px;
    .page-banner {
        position: relative;
        padding-top: 38%;
        background-color: ${({theme}) => theme.mainColor};
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
    }
    .name-series {
        margin: 28px 0 25px 0;
        display: flex;
        justify-content: center;
        text-align: center;
        font-size: 20px;
        line-height: 29px;
        letter-spacing: 0.07em;
        font-weight: 300;
        text-transform: uppercase;
    }
    .photos-wrap {
        display: flex;
        flex-direction: column;
        &__img {
            margin-bottom: 25px;
            &:last-child {
                margin-bottom: 0;
            }
        }
    }
    .button {
        margin: 25px auto 0 auto;
        padding: 8px 40px;
        display: flex;
        align-items: center;
        border: 1px solid ${({theme}) => theme.minorColor};
        border-radius: 5px;
        background-color: transparent;
        box-sizing: border-box;
        transition: background-color .3s, border .3s;
        &__text {
            margin-right: 4px;
            font-size: 18px;
            line-height: 27px;
            color: ${({theme}) => theme.minorColor};
            transition: color .3s;
        }
        &__svg {
            
        }
        path {
            transition: fill .3s
        }
    }
    .video {
        margin-top: 30px;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding-top: 81px;
        .button {
            &:hover {
                border-color: ${({theme}) => theme.mainColor};
                background-color: ${({theme}) => theme.mainColor};
                .button__text {
                    color: #fff;
                }
                path {
                    fill: #fff;
                }
            }
        }
        .name-series {
            margin: 32px 0;
            font-size: 24px;
            line-height: 35px;
            span {
                position: relative;
                &::after,
                &::before {
                    content: '';
                    position: absolute;
                    display: block;
                    top: 15px;
                    width: 170px;
                    height: 1px;
                    background-color: ${({theme}) => theme.minorColor};
                    
                }
                &::after {
                    right: calc(100% + 15px);
                }
                &::before {
                    left: calc(100% + 15px);
                }
            }
        }
        .video {
            width: 586px;
            margin: 30px auto 0 auto;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .page-banner {
            padding-top: 22%;
        }
        .video {
            width: 740px;
            margin: 30px auto 0 auto;
        }
    }
`;