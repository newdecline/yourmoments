import styled from 'styled-components';

export const StyledPhotoSlider = styled('div')`
  .swiper-button-prev,
  .swiper-button-next {
      background-color: transparent;
      &::after {
          content: '';
      }
      path {
          fill: ${({theme}) => theme.minorColor}
      }
  }
  .swiper-button-prev {
      transform: rotate(180deg);
  }
  .main-slider {
    margin-bottom: 17px;
    .swiper-wrapper {
      flex-direction: column;
    }
    .swiper-slide {
      margin: 12px 0;
    }
    .swiper-button-next,
    .swiper-button-prev {
      display: none;
    }
  }
  .thumbnail-slider {
    display: none;
    .swiper-slide {
      position: relative;
      padding-top: 14.4%;
      display: flex;
      align-items: center;
      justify-content: center;
      box-sizing: border-box;
      overflow: hidden;
      &::after {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0);
        transition: background .3s;
      }
      &:not(.swiper-slide-active) {
        &::after {
          background: rgba(0, 0, 0, 0.5);
        }
      }
    }
    img {
      position: absolute;
      top: 50%;
      left: 50%;
      height: 100%;
      transform: translate(-50%, -50%);
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    .main-slider {
      .swiper-wrapper {
        flex-direction: row;
      }
      .swiper-slide {
        padding: 0 54px;
        box-sizing: border-box;
      }
      .inner-slide {
        margin: 0;
        position: relative;
        height: 367px;
        display: flex;
        align-items: center;
        justify-content: center;
        box-sizing: border-box;
        overflow: hidden;
      }
      .swiper-button-next,
      .swiper-button-prev {
        display: block;
      }
      .swiper-button-next {
        right: 0;
      }
      .swiper-button-prev {
        left: 0;
      }
      img {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        object-fit: contain;
      }
    }
    .thumbnail-slider {
      display: block;
      .swiper-slide {
        padding-top: 10.4%;
      }
      img {
        object-fit: cover;
      }
    }
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
    .main-slider {
      margin-bottom: 22px;
      .inner-slide {
        height: 600px;
      }
    }
    .thumbnail-slider {
      .swiper-slide {
        padding-top: 9.3%;
      }
    }
  }
`;