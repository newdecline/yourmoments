import React, {useEffect, useState} from 'react';
import {StyledPhotoSlider} from "./styled";
import PropTypes from 'prop-types';
import ArrowIcon from '../../../../svg/arrow.svg';
import Swiper from "react-id-swiper";
import LazyLoad from 'react-lazyload';
import RollingSVG from '../../../../svg/rolling.svg';

export const PhotoSlider = props => {
  const {images} = props;

  const [swiper, setSwiper] = useState(null);

  const [thumbnailSwiper, setThumbnailSwiper] = useState(null);

  const settingsSlider = {
    getSwiper: setSwiper,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    renderPrevButton: () => <button className="swiper-button-prev"><ArrowIcon/></button>,
    renderNextButton: () => <button className="swiper-button-next"><ArrowIcon/></button>,
    on: {
      init() {
        if (window.matchMedia("(max-width: 1023px)").matches) {
          this.destroy(false, true);
        }
      }
    },
  };

  const settingsThumbnailSlider = {
    getSwiper: setThumbnailSwiper,
    spaceBetween: 20,
    centeredSlides: true,
    touchRatio: 0.2,
    slideToClickedSlide: true,
    breakpoints: {
      1366: {
        slidesPerView: 6.53
      },
      1024: {
        slidesPerView: 5.87
      }
    },
  };

  useEffect(() => {
    if (swiper && thumbnailSwiper) {
      swiper.controller.control = thumbnailSwiper;
      thumbnailSwiper.controller.control = swiper;
    }
  }, [swiper, thumbnailSwiper]);

  return <StyledPhotoSlider>
    <div className="container">
      <div className="main-slider">
        <Swiper {...settingsSlider}>
          {images.map(item => {
            return <div key={item.imageId}>
              <div className='inner-slide'>
                <LazyLoad placeholder={<span><RollingSVG/></span>}>
                  <img src={`${item.path}`} alt="/"/>
                </LazyLoad>
              </div>
            </div>
          })}
        </Swiper>
      </div>
    </div>

    <div className="thumbnail-slider">
      <Swiper {...settingsThumbnailSlider}>
        {images.map(item => <div key={item.imageId}><img src={`${item.path}`} alt="/"/></div>)}
      </Swiper>
    </div>
  </StyledPhotoSlider>
};

PhotoSlider.propTypes = {
  images: PropTypes.array
};