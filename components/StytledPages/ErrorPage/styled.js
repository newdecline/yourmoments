import styled from 'styled-components';

export const StyledErrorPage = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background: #EACDC4;
  overflow: hidden;
  .frame-container {
    width: 313px;
    padding: 64px 31px;
    background-image: url('/assets/images/photo-frame.jpg');
    background-size: cover;
    background-position: center;
    box-sizing: border-box;
  }
  .img {
    display: block;
    width: 250px;
  }
  .text {
    font-size: 22px;
    line-height: 32px;
    text-align: center;
    color: #fff;
  }
  .text_mobile {
    max-width: 323px;
    margin: 13px auto 0 auto;
  }
  .text_desktop {
    display: none;
  }
  @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
    .text_mobile {
      display: none;
    }
    .text_desktop {
      max-width: 323px;
      display: block;
      margin: 11px auto 0 auto;
      color: #898989;
    }
    .img {
      width: 408px;
      margin: 0 auto;
    }
    .frame-container {
      width: 632px;
      padding: 110px 31px;
    }
  }
`;