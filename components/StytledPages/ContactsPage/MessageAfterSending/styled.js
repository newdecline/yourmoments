import styled from 'styled-components';

export const StyledMessageAfterSending = styled('div')`
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    pointer-events: ${({submitted}) => submitted ? 'unset' : 'none'};
    z-index: 12;
    .popup-form-overlay {
        display: flex;
        align-items: center;
        justify-content: center;
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: #fff;
        color: ${({theme}) => theme.minorColor};
    }
    .inner {
        max-width: 80%;
        padding: 30px 15px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        font-size: 22px;
        line-height: 32px;
        text-align: center;
        letter-spacing: 0.07em;
        box-sizing: border-box;
        &__img {
            width: 260px;
            margin-bottom: 30px;
        }
        &__title {
            margin: 0 0 10px 0;
            font-family: 'Circe-Bold', sans-serif;
            span {
                text-transform: uppercase;
            }
        }
        &__subtitle {
            margin: 0 0 36px 0;
            font-size: 20px;
            line-height: 29px;
            font-family: 'Circe-Light', sans-serif;
        }
        &__close {
            padding: 8px 70px;
            margin: 0;
            border: ${({theme}) => `1px solid ${theme.minorColor}`};
            background-color: transparent;
            font-size: 18px;
            line-height: 27px;
            font-family: 'Circe-Light', sans-serif;
            color: ${({theme}) => theme.minorColor};
            box-sizing: border-box;
            border-radius: 5px;
        }
        &.inner_error {
            padding: 150px 0 120px 0;
            .inner__close {
                position: absolute;
                top: 30px;
                left: 30px;
                display: flex;
                padding: 0;
                border: none;
            }
            .inner__subtitle {
                max-width: 380px;
            }
        }
        .social-list {
            padding: 0;
            margin: 0;
            display: flex;
            list-style-type: none;
            &__item {
                margin-right: 30px;
                a {
                    display: flex;
                }
                svg {
                    width: 40px;
                    height: auto;
                }
                &:last-child {
                    margin-right: 0;
                }
                circle {
                    fill: ${({theme}) => theme.minorColor}
                }
                path {
                    fill: #fff
                }
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .inner {
            padding: 30px 0 50px 0;
            width: 796px;
            border: ${({theme}) => `3px solid ${theme.mainColor}`};
            &__title {
                span {
                    text-transform: unset;
                }
            }
            &.inner_success {
                .inner__close {
                    transition: background-color .3s, color .3s, border .3s;
                    &:hover {
                        cursor: pointer;
                        background-color: ${({theme}) => theme.mainColor};
                        border: 1px solid transparent;
                        color: #fff;
                    }
                }
            }
            &.inner_error {
                position: relative;
                .inner__close {
                    left: unset;
                    right: 30px;
                    &:hover {
                        cursor: pointer;
                    }
                }
            }
        }
    }
`;