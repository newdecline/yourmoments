import React from 'react';
import {inject, observer} from 'mobx-react';
import {CSSTransition} from "react-transition-group";
import {StyledMessageAfterSending} from "./styled";
import MailIcon from "../../../../svg/mail-with-circle.svg";
import VkIcon from "../../../../svg/vk-with-circle.svg";
import InstagramIcon from "../../../../svg/instagram-with-circle.svg";
import CrossRotation from "../../../../svg/cross-rotate.svg";
import {CommunicationMethodList} from "../../../CommunicationMethodList/CommunicationMethodList";

export const MessageAfterSending = inject('appStore')(observer(props => {
    const {submitted, onCloseMessageAfterSending, errorSubmitted, appStore} = props;
    const {throughData} = appStore;

    const handleClickCloseBtn = () => {
        onCloseMessageAfterSending();
    };

    const renderSuccessMessage = () => {
        return (
            <div className="inner inner_success">
                <img className='inner__img' src="/assets/images/flowers.jpg" alt="/"/>
                <p className='inner__title'><span>Спасибо!</span> Форма&nbsp;успешно&nbsp;отправлена.</p>
                <p className='inner__subtitle'>Мы свяжемся с вами)</p>
                <button
                    onClick={handleClickCloseBtn}
                    className="inner__close">Закрыть</button>
            </div>
        )
    };

    const renderErrorMessage = () => {
        return (
            <div className="inner inner_error">
                <button
                    onClick={handleClickCloseBtn}
                    className="inner__close"><CrossRotation/> </button>
                <p className='inner__title'><span>Извините!</span> Форма&nbsp;не&nbsp;отправлена...</p>
                <p className='inner__subtitle'>Пожалуйста, свяжитесь с нами
                    другим удобным способом)</p>
                <CommunicationMethodList/>
            </div>
        )
    };

    return <StyledMessageAfterSending submitted={submitted}>
        <CSSTransition
            in={submitted}
            timeout={1000}
            classNames="popup-form"
            unmountOnExit>
            <div className='popup-form-overlay'>
                {
                    !errorSubmitted
                        ? renderSuccessMessage()
                        : renderErrorMessage()
                }
            </div>
        </CSSTransition>
    </StyledMessageAfterSending>
}));