import styled from "styled-components";

export const StyledPage = styled('div')`
    padding-top: 67px;
    .wrapper-content {
        display: flex;
        flex-direction: column;
    }
    .top-banner {
        position: relative;
        padding-top: 117.9%;
        overflow: hidden;
        picture {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding-top: 81px;
        .top-banner {
            padding-top: 28.9%;
        }
        .wrapper-content {
            margin-top: 43px;
            flex-direction: row;
            justify-content: space-between;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .top-banner {
            padding-top: 21.7%;
        }
        .wrapper-content {
            padding: 0 150px;
        }
    }
`;

export const StyledLeftColumnForm = styled('div')`
    padding: 0 30px;
    .title {
        margin: 28px 0 8px 0;
        text-align: center;
        font-size: 24px;
        line-height: 35px;
        text-transform: uppercase;
        font-family: 'Circe-Light';
        font-weight: 300;
    }
    .contact-person {
        margin-bottom: 25px;
        text-align: center;
        &__text {
            margin: 0 0 15px 0;
            font-size: 16px;
            line-height: 24px;
            text-align: center;
        }
        &__wrap {
            
        }
        &__phone {
            text-decoration: none;
            font-size: 24px;
            line-height: 35px;
            letter-spacing: 0.07em;
            font-family: 'Circe-Bold';
        }
        &__name {
            font-size: 20px;
            line-height: 29px;
        }
    }
    .location-note {
        position: relative;
        padding-left: 26px;
        margin-bottom: 28px;
        box-sizing: border-box;
        &__icon {
            position: absolute;
            top: 0;
            left: 0;
        }
        &__text {
            font-size: 14px;
            line-height: 21px;
        }
    }
    .communication-method-list {
        margin: 0 0 34px 0;
        position: relative;
        z-index: 1;
        &__item {
            margin-right: 30px;
            &:last-child {
                margin-right: 0;
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding: 0;
        max-width: 318px;
        box-sizing: border-box;
        .title {
            margin: 0 0 30px 0;
            text-align: left;
        }
        .contact-person {
            margin-bottom: 45px;
            &__text {
                text-align: left;
            }
            &__wrap {
                text-align: left;
            }
        }
        .communication-method-list {
            justify-content: flex-start;
        }
        .link svg {
            width: 40px;
            height: auto;
            circle {
                transition: fill .3s;
            }
            &:hover {
                circle {
                    fill: ${({theme}) => theme.mainColor};
                }
                path {
                  fill: #fff;
                }
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        .contact-person {
            margin-bottom: 30px;
        }
        .location-note {
            margin-top: 37px;
        }
    }
`;
export const StyledRightColumnForm = styled('div')`
    padding: 0 15px;
    display: flex;
    flex-direction: column;
    .title {
        margin-bottom: 15px;
        text-align: center;
        font-size: 24px;
        line-height: 35px;
        text-transform: uppercase;
        font-family: 'Circe-Light';
        font-weight: 300;
        &__br:last-child {
            display: none;
        }
    }
    .form {
        display: flex;
        flex-direction: column;
        &__input {
            width: 100%;
            padding: 12px 12px 12px 22px;
            margin-bottom: 11px;
            border: 1px solid #898989;
            box-sizing: border-box;
            border-radius: 5px;
            font-size: 14px;
            line-height: 21px;
            letter-spacing: 0.04em;
            &:last-of-type {
                margin-bottom: 0;
            }
            &::placeholder {
                color: #c4c4c4;
            }
            &.error {
                border: 1px solid #D10000;
            }
        }
        &__label-checkbox {
            margin: 15px 0;
            &.error {
                .label-checkbox-text {
                    color: #D10000;
                    &::after {
                        border: 1px solid #D10000;
                    }
                }
            }
        }
        &__submit-button {
            margin: 15px 0 0 0;
            padding: 13px 0;
            width: 100%;
            background-color: ${({theme}) => theme.mainColor};
            color: #fff;
            border-radius: 5px;
            box-sizing: border-box;
            &:disabled {
                opacity: .6;
            }
        }
    }
    .label-checkbox-input {
        position: absolute;
        z-index: -1;
        visibility: hidden;
        &:checked + .label-checkbox-text::after {
            background-color: ${({theme}) => theme.mainColor};
            border: none;
            
        }
    }
    .label-checkbox-text {
        position: relative;
        padding-left: 30px;
        font-size: 12px;
        line-height: 22px;
        letter-spacing: 0.14em;
        box-sizing: border-box;
        transition: color .3s;
        &::after {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            width: 20px;
            height: 20px;
            border: 1px solid ${({theme}) => theme.minorColor};
            border-radius: 50%;
            box-sizing: border-box;
            transition: background-color .3s;
        }
    }
    .captcha-wrap {
        display: flex;
        align-items: center;
        justify-content: center;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        padding: 0;
        max-width: 335px;
        box-sizing: border-box;
        .title {
            text-align: left;
            &__br:first-child {
                display: none;
            }
        }
        .form {
            &__input {
                margin-bottom: 21px;
            }
            &__label-checkbox {
                cursor: pointer;
            }
            &__submit-button {
                transition: background-color .3s;
                &:hover {
                    cursor: pointer;
                    background-color: #D5AFA4;
                }
            }
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        max-width: 427px;
        .captcha-wrap {
            justify-content: flex-start;
        }
        .label-checkbox-text {
            font-size: 15px;
            letter-spacing: 0.141rem;
        }
    }
`;
