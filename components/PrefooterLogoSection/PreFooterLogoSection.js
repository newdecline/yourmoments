import React from "react";
import {StyledPreFooterLogoSection} from "./styled";
import Logo from "../../svg/logo.svg";
import LogoWithLines from "../../svg/logo-with-lines.svg";
import PreFooterLogo from "../../svg/pre-footer-logo.svg";

export const PreFooterLogoSection = () => {
    return (
        <StyledPreFooterLogoSection>
            <Logo className='logo-svg'/>
            <LogoWithLines className='logo-svg-with-lines'/>
            <PreFooterLogo className='pre-footer-logo'/>
        </StyledPreFooterLogoSection>
    )
};