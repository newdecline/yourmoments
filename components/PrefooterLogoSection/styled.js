import styled from "styled-components";

export const StyledPreFooterLogoSection = styled('div')`
    padding: 40px 0;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
    .logo-svg {
        display: block;
    }
    .logo-svg-with-lines, .pre-footer-logo {
        display: none;
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.tablet.media}px`}) {
        .logo-svg {
            display: none;
        }
        .logo-svg-with-lines {
            display: flex;
            width: 100%;
            height: auto;
        }
    }
    @media (min-width: ${({theme}) => `${theme.breakPoints.desktop.media}px`}) {
        padding: 60px 0;
        display: block;
        text-align: center;
        overflow: hidden;
        .logo-svg-with-lines {
            display: none;
        }
        .pre-footer-logo {
            position: relative;
            display: flex;
            width: unset;
            height: 177px;
            left: 50%;
            transform: translateX(-50%);
        }
    }
`;