import {withRouter} from "next/router";
import Link from "next/link";
import React, {Children} from "react";
import PropTypes from "prop-types";

const ActiveLink = ({router, children, ...props}) => {
    const child = Children.only(children);

    let className = child.props.className || "";

    const test = router.asPath.lastIndexOf('/');

    if (
        (router.asPath.replace(/#(.*)/g, "") === props.as) &&
        props.activeClassName
    ) {
        className = `${className} ${props.activeClassName}`.trim();
    }

    if (router.asPath.substr(0, test) === props.as) {
        if (
            props.as === '/portfolio/paired-photos'
            || props.as === '/portfolio/wedding-photos'
            || props.as === '/portfolio/family-photos') {
            className = `${className} ${props.activeClassName}`.trim();
        }
    }

    delete props.activeClassName;

    return <Link {...props}>{React.cloneElement(child, {className})}</Link>;
};

export default withRouter(ActiveLink);

ActiveLink.propTypes = {
    router: PropTypes.object,
    children: PropTypes.object,
    activeClassName: PropTypes.string,
    as: PropTypes.string
};