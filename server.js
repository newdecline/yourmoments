const express = require('express');
const next = require('next');
const proxy = require('http-proxy-middleware');

const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();
const port = parseInt(process.env.PORT, 10) || 3000;

const optionsProxy = proxy({
    target: process.env.BASE_URL,
    changeOrigin: true
});

app.prepare().then(() => {
    const server = express();

    server.use(['/api', '/uploads', '/images'], optionsProxy);

    server.all('*', (req, res) => {
        return handle(req, res);
    });

    const listenCallback = err => {
        if (err) throw err;
        console.log(`> Ready on http://localhost:${port}`)
    };

    dev ? server.listen(port, listenCallback) : server.listen(port, 'localhost', listenCallback);
})
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });