import React, {useState, useEffect} from "react";
import $ from "jquery";
import "../public/assets/scripts/jquery.instagramFeed.min";
import {observer} from "mobx-react";
import {StyledPage, StyledWatchUsOnInstagram} from "../components/StytledPages/IndexPage/styled";
import {TopBannerSlider} from "../components/StytledPages/IndexPage/TopBannerSlider/TopBannerSlider";
import {PreFooterLogoSection} from "../components/PrefooterLogoSection/PreFooterLogoSection";
import Head from "next/head";
import {MainLayout} from "../components/Layouts/MainLayout";
import {fetchApi} from "../utilities/fetchApi";

const Page = observer(props => {
  const {
    layoutData,
    pageData: {
      banner
    }
  } = props;

  const [posts, setPosts] = useState([]);

  useEffect(() => {
    fetchPosts().then(posts => {
      setPosts(posts);
    })
  }, []);

  const fetchPosts = () => new Promise((resolve, reject) => {
    $.instagramFeed({
      'username': 'lenphoto.ru',
      'get_data': true,
      'callback': function (data) {
        const posts = data.edge_owner_to_timeline_media.edges.map((post, i) => {
          return {
            previewUrl: data.edge_owner_to_timeline_media.edges[i].node.thumbnail_src,
            postUrl: `https://www.instagram.com/p/${data.edge_owner_to_timeline_media.edges[i].node.shortcode}`
          }
        });

        resolve(posts);
      }
    });
  });

  const renderInstagramPosts = () => {
    if (posts.length === 0) {
      return (
        <div className='preloader'>Загрузка постов...</div>
      )
    }

    return (
      <div className="instagram">
        {posts.slice(0, 8).map(post => (
          <a
            key={post.postUrl}
            href={post.postUrl}
            target='_blank'
            rel='noreferrer noopener'
            className="instagram__link">
            <img src={post.previewUrl} alt='/'/>
          </a>
        ))}
      </div>
    )
  };

  return <>
    <Head>
      <title>Фотограф | Видеограф</title>
      <meta name="description"
            content="Фотограф и Видеограф! Съемка теплых и радостных событий по всему миру! Снимаем парные, свадебные и семейные фотосессии."/>
    </Head>

    <StyledPage>
      {banner.length !== 0 && <TopBannerSlider banner={banner}/>}

      <StyledWatchUsOnInstagram>
        <h3 className='title'>Смотрите нас в инстаграмм</h3>

        {layoutData.instagram && (
          <a className='link' href={layoutData.instagram} rel='noopener noreferrer' target='_blank'>lenphoto.ru</a>
        )}

        <div className="container">
          {renderInstagramPosts()}
        </div>
      </StyledWatchUsOnInstagram>
      <PreFooterLogoSection/>
    </StyledPage>
  </>
});

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi('/frontend/home-page')]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};
    const pageData = data[1].data;

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
      pageData
    }
  }
};

export default Page;