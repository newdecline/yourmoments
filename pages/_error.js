import React from "react";
import {StyledErrorPage} from "../components/StytledPages/ErrorPage/styled";

// eslint-disable-next-line react/prop-types
function Error() {
  return (
    <StyledErrorPage>
      <div className="frame-container">
        <img className='img' src="/assets/images/404.png" alt="/"/>
        <p className="text text_desktop">Страница устарела, была удалена или не существовала вовсе</p>
      </div>
      <p className="text text_mobile">Страница устарела, была удалена или не существовала вовсе</p>
    </StyledErrorPage>
  )
}

Error.getInitialProps = ({res, err}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return {statusCode}
};

export default Error