import React, {useState, useEffect} from 'react';
import {inject, observer} from 'mobx-react';
import {StyledPage, StyledSlide, StyledSlider} from "../components/StytledPages/AboutPage/styled";
import {PreFooterLogoSection} from "../components/PrefooterLogoSection/PreFooterLogoSection";
import Swiper from "react-id-swiper";
import ArrowIcon from "../svg/arrow.svg";
import Head from "next/head";
import {MainLayout} from "../components/Layouts/MainLayout";
import {fetchApi} from "../utilities/fetchApi";

const Page = inject('appStore')(observer(props => {
  const {appStore} = props;

  const [swiper, setSwiper] = useState(null);
  const [reachBeginning, setReachBeginning] = useState(true);
  const [reachEnd, setReachEnd] = useState(false);

  useEffect(() => {
    if (swiper !== null) {
      swiper.on('slideChange', () => {
        setReachBeginning(swiper.isBeginning);
        setReachEnd(swiper.isEnd);
      });

      if (window.matchMedia("(min-width: 1024px)").matches) {
        swiper.destroy();
      }
    }
  }, [swiper]);

  const settingsSlider = {
    getSwiper: setSwiper
  };

  const goNext = () => {
    if (swiper !== null) {
      swiper.slideNext();
    }
  };

  const goPrev = () => {
    if (swiper !== null) {
      swiper.slidePrev();
    }
  };

  return <>
    <Head>
      <title>О нас</title>
      <meta name="description"
            content="Мы фотограф и видеограф, а еще мы МУЖ и ЖЕНА! У нас нет шаблонов и запретов, Мы всегда открыты для общения и готовы к Вашим идеям! Будь то необычный образ или съемка в другой стране"/>
    </Head>

    <StyledPage>
      <div className="container">
        <h3 className="title">Мы фотограф и видеограф, а еще мы МУЖ и ЖЕНА!</h3>
        <StyledSlider>
          <Swiper {...settingsSlider}>
            {appStore.aboutImages.map((item, index) => (
              <StyledSlide key={index} className={`my-slide my-slide-${index + 1}`}>
                <div
                  style={{
                    backgroundImage: `url(${item})`
                  }}
                  className={`img-wrap img-wrap-${index + 1}`}>
                </div>
              </StyledSlide>
            ))}
          </Swiper>
          <button
            disabled={reachBeginning}
            className="swiper-button-prev"
            onClick={goPrev}><ArrowIcon/></button>
          <button
            disabled={reachEnd}
            className="swiper-button-next"
            onClick={goNext}><ArrowIcon/></button>
        </StyledSlider>
        <div className="paragraph-wrap">
          <p className="paragraph">
            Работа в паре позволяет нам видеть целостную
            картинку: душевную, искреннюю, эмоциональную... из которой
            рождаются свадебные фильмы и фотоальбомы,
            а в последствии ценные воспоминания
            и семейные реликвии.
          </p>
          <p className="paragraph">
            Никаких конфликтов - все легко и комфортно. Мы никогда не заставим вставать в неудобные позы,
            показывать
            наигранные эмоции, или делать то, чего Вам не хочется...&nbsp;<span className="paragraph-text-wrap"> Давайте прислушаемся к друг другу и создадим
                    Вашу историю? Которая спустя долгие годы все также будет радовать Вас!</span>
          </p>
          <p className="paragraph paragraph_desktop-visible">Давайте прислушаемся к друг другу и создадим
            Вашу историю? Которая спустя долгие годы все также будет радовать Вас!</p>
          <p className="paragraph">
            У нас нет шаблонов и запретов,
            Мы всегда открыты для общения
            и готовы
            к Вашим идеям!
            Будь то необычный образ
            Или съемка в другой стране...
          </p>
        </div>
      </div>
      <PreFooterLogoSection/>
    </StyledPage>
  </>
}));

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements')]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
    }
  }
};

export default Page;