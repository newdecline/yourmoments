import React, {useState, useEffect} from 'react';
import Swiper from 'react-id-swiper';
import { Scrollbars } from 'react-custom-scrollbars';
import {observer } from 'mobx-react';
import {StyledPage, StyledSlide} from "../components/StytledPages/ReviewsPage/styled";
import ArrowIcon from "../svg/arrow.svg";
import {PreFooterLogoSection} from "../components/PrefooterLogoSection/PreFooterLogoSection";
import Head from "next/head";
import {MainLayout} from "../components/Layouts/MainLayout";
import {fetchApi} from "../utilities/fetchApi";

const Page = observer(props => {
  const {
    pageData
  } = props;

  const reviews = pageData;

  const [heightScrollbars, setHeightScrollbars] = useState(257);
  useEffect(() => {
    if (window.matchMedia("(min-width: 1024px)").matches) {
      setHeightScrollbars(189);
    }
  }, []);

  const settingsSlider = {
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true,
      dynamicBullets: true,
      dynamicMainBullets: 5,
    },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    renderPrevButton: () => <button className="swiper-button-prev"><ArrowIcon/></button>,
    renderNextButton: () => <button className="swiper-button-next"><ArrowIcon/></button>,
  };

  return <>
    <Head>
      <title>Отзывы</title>
      <meta name="description"
            content="Отзывы наших клиентов"/>
    </Head>

    <StyledPage>
      <h6 className="page-title">Отзывы наших клиентов</h6>
      <div className="container">
        {reviews.length === 0 && <h2>Пока нет отзывов</h2>}
        <div className="slider-wrap">
          <Swiper {...settingsSlider}>
            {reviews.map((review, i) => {
              return (
                <StyledSlide key={i}>
                  <div className="inner-slide">
                    <div className="img-wrap">
                      {review.croppedImage && <img src={review.croppedImage.path} alt="/"/>}
                    </div>
                    <div className="wrapper">
                      <Scrollbars
                        renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                        className='scrollbars'
                        style={{height: heightScrollbars}}
                        universal={true}>
                        <div className="reviews-text" dangerouslySetInnerHTML={{__html: review.descriptionHTML}}/>
                      </Scrollbars>
                      <div className="reviews-author">{review.name}</div>
                    </div>
                  </div>
                </StyledSlide>
              )
            })}
          </Swiper>
        </div>
      </div>
      <PreFooterLogoSection/>
    </StyledPage>
  </>
});

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi('/frontend/reviews-page')]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};
    const pageData = data[1].data;

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
      pageData
    }
  }
};

export default Page;