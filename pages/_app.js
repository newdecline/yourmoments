import React from 'react';
import App from 'next/app';
import {configure} from 'mobx';
import {withMobx} from 'next-mobx-wrapper';
import {Provider, useStaticRendering} from 'mobx-react';
import {GlobalStyle} from "../globalStyles";
import {Normalize} from 'styled-normalize';
import {Theme} from "../Theme";

import * as getStores from '../stores';

const isServer = !process.browser;
const Noop = ({children}) => children;

configure({enforceActions: 'observed'});
useStaticRendering(isServer);

class MyApp extends App {

  render() {
    const {Component, pageProps, store} = this.props;
    const Layout = Component.Layout || Noop;

    return (
      <Provider {...store}>
        <Theme>
          <Normalize/>
          <GlobalStyle/>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </Theme>
      </Provider>
    )
  }
}

export default withMobx(getStores)(MyApp);