import React from 'react';
import {observer, inject} from 'mobx-react';
import {StyledModuleCard, Styled} from "../components/StytledPages/TrainingPage/styled";
import MailIcon from "../svg/mail-with-circle.svg";
import VkIcon from "../svg/vk-with-circle.svg";
import InstagramIcon from "../svg/instagram-with-circle.svg";
import {PreFooterLogoSection} from "../components/PrefooterLogoSection/PreFooterLogoSection";
import Head from "next/head";
import {MainLayout} from "../components/Layouts/MainLayout";
import {fetchApi} from "../utilities/fetchApi";

const topBannerBg = 'assets/images/training/top-banner.jpg';

const Page = inject('appStore')(observer(props => {
  const {
    layoutData,
    pageData,
  } = props;

  const {lessons} = pageData;

  const hasCommunicationMethods = layoutData.email && layoutData.vkontakte && layoutData.instagram;

  return <>
    <Head>
      <title>Уроки по фотографии</title>
      <meta name="description"
            content="Уроки по фотографии онлайн. Видеоуроки, которые станут проводниками в мир коммерческой фотографии. Обратная связь и 5 авторских пресетов в подарок!"/>
    </Head>

    <Styled bannerBg={topBannerBg}>
      <div className="top-banner"/>
      <div className="container">
        <div className="page-header">
          {pageData.title && <div className="page-header__title">
            <span className='page-header__title-inner'>{pageData.title}</span>
          </div>}
          {pageData.subtitle1HTML && <div className="page-header__subtitle" dangerouslySetInnerHTML={{__html: pageData.subtitle1HTML}}/>}
          {pageData.subtitle2HTML && <div className="page-header__subtitle-2" dangerouslySetInnerHTML={{__html: pageData.subtitle2HTML}}/>}
        </div>
        <div className="card-wrap">
          {lessons.map((item, i) => (
            <StyledModuleCard key={i}>
              <div className="number-module">{item.name}</div>
              <div className='lesson-title'>{item.title}</div>
              <div className='lesson-subtitle'>{item.subtitle}</div>
              <div className="description-list" dangerouslySetInnerHTML={{__html: item.descriptionHTML}}/>
            </StyledModuleCard>
          ))}
        </div>

        {pageData.costOfAllModules && <div className="cost">
          Стоимость всех 4-х модулей
          <span className='cost__divider'> — </span>
          <span className="cost__value">{pageData.costOfAllModules}
            <span className="cost__currency"> рублей</span>
                </span>
        </div>}

        <div className="contact-us">
          {pageData.note && <div className="contact-us__title">{pageData.note}</div>}

          {hasCommunicationMethods && <ul className="social-list">
            <li className="social-list__item">
              {layoutData.email && <a
                href={`mailto:${layoutData.email}`}
                className='social-link'
                target='_blank'
                rel='noopener noreferrer'><MailIcon/></a>}
            </li>
            <li className="social-list__item">
              {layoutData.vkontakte && <a
                href={layoutData.vkontakte}
                className='social-link'
                target='_blank'
                rel='noopener noreferrer'><VkIcon/></a>}
            </li>
            <li className="social-list__item">
              {layoutData.instagram && <a
                href={layoutData.instagram}
                className='social-link'
                target='_blank'
                rel='noopener noreferrer'><InstagramIcon/></a>}
            </li>
          </ul>}
        </div>
      </div>
      <PreFooterLogoSection/>
    </Styled>
  </>
}));

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi('/frontend/training-page')]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};
    const pageData = data[1].data;

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
      pageData
    }
  }
};

export default Page;