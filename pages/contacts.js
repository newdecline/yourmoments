import React, {useState, useEffect, useRef} from 'react';
import {Portal} from 'react-portal';
import {fetchApi} from '../utilities/fetchApi';
import {
  StyledPage,
  StyledLeftColumnForm,
  StyledRightColumnForm
} from "../components/StytledPages/ContactsPage/styled";
import LocationIcon from '../svg/location.svg';
import {useForm} from "react-hook-form";
import ReCAPTCHA from "react-google-recaptcha";
import {PreFooterLogoSection} from "../components/PrefooterLogoSection/PreFooterLogoSection";
import {MessageAfterSending} from "../components/StytledPages/ContactsPage/MessageAfterSending/MessageAfterSending";
import {disablePageScroll, enablePageScroll} from "scroll-lock";
import {CommunicationMethodList} from "../components/CommunicationMethodList/CommunicationMethodList";
import Head from "next/head";
import {MainLayout} from "../components/Layouts/MainLayout";

const Page = props => {
  const {layoutData} = props;

  const gReCaptchaRef = useRef(null);

  const [loading, setLoading] = useState(false);
  const [errorSubmitted, setErrorSubmitted] = useState(false);

  const [submitted, setSubmitted] = useState(false);
  useEffect(() => {
    if (submitted) {
      disablePageScroll();
    } else {
      enablePageScroll();
    }
  }, [submitted]);

  const {register, handleSubmit, setValue, errors, setError, clearError, reset} = useForm();

  const onCloseMessageAfterSending = () => {
    setSubmitted(false);
  };

  const onSubmit = async dataForm => {
    setLoading(true);
    delete dataForm.agreement;

    const {status} = await fetchApi('/feedback-form', {
      method: 'post',
      body: JSON.stringify(dataForm)
    });

    setSubmitted(true);

    if (status !== 200) {
      setErrorSubmitted(true);
    }

    if (gReCaptchaRef.current) {
      gReCaptchaRef.current.reset();
    }

    reset();
    setLoading(false);
  };

  const onChange = value => {
    setValue('captcha', value);
    clearError('captcha');
    if (!value) {
      setError('captcha', value);
    }
  };

  useEffect(() => {
    register({name: 'captcha'}, {required: true})
  }, []);

  return <>
    <Head>
      <title>Контакты</title>
      <meta name="description"
            content="Решить вопросы, касательно фотографии, посоветоваться, забронировать дату. Решить вопросы по видеосъемке, техническим аспектам, назначить встречу."/>
    </Head>

    <StyledPage>
      <div className="top-banner">
        <picture>
          <source srcSet="assets/images/contacts/mobile-img.jpg" media="(max-width: 1023px)"/>
          <source srcSet="assets/images/contacts/tablet-img.jpg" media="(max-width: 1365px)"/>
          <img src="assets/images/contacts/desktop-img.jpg" alt="bg"/>
        </picture>
      </div>
      <div className="container">
        <div className="wrapper-content">
          <StyledLeftColumnForm>
            <div className="title">Наши контакты</div>
            <div className="contact-person">
              <div className="contact-person__text">Решить вопросы, касательно фотографии, посоветоваться,
                забронировать дату...
              </div>
              <div className="contact-person__wrap">
                <a href="tel:+79517912393" className="contact-person__phone">{layoutData.phone1} </a>
                <span className="contact-person__name">/ Елена</span>
              </div>
            </div>
            <div className="contact-person">
              <div className="contact-person__text">Решить вопросы по видеосъемке, техническим аспектам,
                назначить встречу...
              </div>
              <div className="contact-person__wrap">
                <a href="tel:+79124795228" className="contact-person__phone">{layoutData.phone2} </a>
                <span className="contact-person__name">/ Данил</span>
              </div>
            </div>
            <div className="location-note">
              <LocationIcon className="location-note__icon"/>
              <div className="location-note__text">Мы любим путешествовать и с радостью
                отснимем ваше торжество в другом городе
                или стране
              </div>
            </div>
            <CommunicationMethodList className='communication-method-list'/>
          </StyledLeftColumnForm>
          <StyledRightColumnForm>
            <div className="title">Пожалуйста,<br className='title__br'/> заполните<br className='title__br'/> форму</div>
            <form onSubmit={handleSubmit(onSubmit)} id='form' className='form offset-indent-form-anchor'>
              <input
                placeholder='Введите имя'
                className={errors.name ? 'form__input error' : 'form__input'}
                name='name'
                ref={register({required: true})}/>
              <input
                placeholder='Ссылка на соц. сети'
                className={errors.socialNetwork ? 'form__input error' : 'form__input'}
                name='socialNetwork'
                ref={register({required: true})}/>
              <input
                placeholder='Дата съемки'
                className={errors.date ? 'form__input error' : 'form__input'}
                name='date'
                ref={register({required: true})}/>
              <label className={errors.agreement ? 'form__label-checkbox error' : 'form__label-checkbox'}>
                <input
                  type="checkbox"
                  name='agreement'
                  ref={register({required: true})}
                  className='label-checkbox-input'/>
                <div className="label-checkbox-text">Согласие на обработку персональных данных</div>
              </label>
              <div className="captcha-wrap">
                <ReCAPTCHA
                  ref={gReCaptchaRef}
                  size='normal'
                  sitekey={process.env.GOOGLE_CAPTCHA}
                  onChange={onChange}/>
              </div>
              <button
                disabled={Object.keys(errors).length}
                className="form__submit-button"
                type='submit'>{loading ? 'Бронируется...' : 'Забронировать'}
              </button>
            </form>
          </StyledRightColumnForm>
        </div>
      </div>
      <PreFooterLogoSection/>
      <Portal>
        <MessageAfterSending
          onCloseMessageAfterSending={onCloseMessageAfterSending}
          errorSubmitted={errorSubmitted}
          submitted={submitted}/>
      </Portal>
    </StyledPage>
  </>
};

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements')]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
    }
  }
};

export default Page;