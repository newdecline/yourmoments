import React from 'react';
import {StyledPage} from "../../../../components/StytledPages/PhotoSeries/styled";
import {PhotoSlider} from "../../../../components/StytledPages/PhotoSeries/PhotoSlider/PhotoSlider";
import PropTypes from 'prop-types';
import {PreFooterLogoSection} from "../../../../components/PrefooterLogoSection/PreFooterLogoSection";
import {BreadCrumbs} from "../../../../components/StytledPages/PhotosSeries/BreadCrumbs/BreadCrumbs";
import {VideoPlayer} from "../../../../components/VideoPlayer/VideoPlayer";
import PlayIcon from "../../../../svg/play.svg";
import Head from "next/head";
import {fetchApi} from "../../../../utilities/fetchApi";
import {MainLayout} from "../../../../components/Layouts/MainLayout";

const Page = props => {
  const {pageData} = props;

  if (props.hasOwnProperty('error')) {
    const err = new Error();
    err.statusCode = 404;
    err.code = 'ENOENT';
    throw err;
  }

  const {series, gallerySeries} = pageData;

  return <>
    <Head>
      <title>Фотосессиия | {series.name}</title>
      <meta name="description"
            content={`Фотосессиия. ${series.name}`}/>
    </Head>

    <StyledPage>
      <div className="container">
        <BreadCrumbs root={{label: 'Портфолио', href: '/portfolio'}} title={series.name}/>
      </div>
      <div className="page-banner" style={{backgroundImage: `url(${series.croppedBannerImage.path})`}}/>
      <h6 className="name-series"><span>{series.name}</span></h6>

      {gallerySeries.length !== 0 && <PhotoSlider images={gallerySeries}/>}

      <div className="container">
        {series.urlVideo && <div className="video">
          <VideoPlayer
            iconPlay={<PlayIcon/>}
            videoURL={series.urlVideo}/>
        </div>}
      </div>
      <PreFooterLogoSection/>
    </StyledPage>
  </>
};

Page.propTypes = {
  pageData: PropTypes.object
};

Page.Layout = MainLayout;

Page.getInitialProps = async ({store, query, res}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi(`/frontend/portfolio-page/${query.category}/${query.slug}`)]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};
    const pageData = data[1].data;

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
      pageData
    }
  } else {
    res.statusCode = 404;
    return {
      error: {
        statusCode: 404,
        message: data.message
      }
    };
  }
};

export default Page;