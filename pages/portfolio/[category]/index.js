import React from 'react';
import {useRouter} from 'next/router';
import {inject, observer} from 'mobx-react';
import PropTypes from 'prop-types';
import {StyledPage} from "../../../components/StytledPages/PhotosSeries/styled";
import Link from "next/link";
import {PreFooterLogoSection} from "../../../components/PrefooterLogoSection/PreFooterLogoSection";
import {BreadCrumbs} from "../../../components/StytledPages/PhotosSeries/BreadCrumbs/BreadCrumbs";
import Head from "next/head";
import {fetchApi} from "../../../utilities/fetchApi";
import {MainLayout} from "../../../components/Layouts/MainLayout";

const Page = inject('portfolioStore')(observer(props => {
  const router = useRouter();
  const {query} = router;
  const {
    pageData: {
      series
    }
  } = props;

  let nameCategory = '';

  switch (query.category) {
    case 'paired':
      nameCategory = 'Парные';
      break;
    case 'wedding':
      nameCategory = 'Свадебные';
      break;
    case 'business-content':
      nameCategory = 'Контент для бизнеса';
      break;
    default:
      nameCategory = null
  }

  return <>
    <Head>
      <title>Портфолио | {nameCategory} фотосессии</title>
      <meta name="description"
            content={`Портфолио. ${nameCategory} фотосессии`}/>
    </Head>

    <StyledPage>
      <div className="container">
        <BreadCrumbs root={{label: 'Портфолио', href: '/portfolio'}}/>

        {series.length === 0
          ? <h3 className='no-series'>Скоро тут появятся фотосерии</h3>
          : <div className="grid-container">
            {series.map(item => {
              return <Link
                href="/portfolio/[category]/[slug]"
                as={`/portfolio/${query.category}/${item.slug}`}
                key={item.slug}>
                <a className="card">
                  <div className="card__img-wrap">
                    <img src={`${item.croppedCardImage.path}`} alt="/" className='card__img'/>
                  </div>
                  <div className="card__name">{item.name}</div>
                </a>
              </Link>
            })}
          </div>}
      </div>
      <PreFooterLogoSection/>
    </StyledPage>
  </>
}));

Page.propTypes = {
  photoSeries: PropTypes.object
};

Page.Layout = MainLayout;

Page.getInitialProps = async ({store, query}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi(`/frontend/portfolio-page/${query.category}`)]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};
    const pageData = data[1].data;

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
      pageData
    }
  }
};

export default Page;