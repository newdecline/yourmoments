import React, {useState, useEffect} from 'react';
import {observer, inject} from 'mobx-react';
import Link from "next/link";
import Swiper from "react-id-swiper";
import PlayIcon from "../../svg/play.svg";
import {PreFooterLogoSection} from "../../components/PrefooterLogoSection/PreFooterLogoSection";
import {StyledPage, StyledSlider} from "../../components/StytledPages/PortfolioPage/styled";
import {VideoPlayer} from "../../components/VideoPlayer/VideoPlayer";
import ArrowIcon from "../../svg/arrow.svg";
import PairedWordIcon from "../../svg/paired-word.svg";
import WeddingWordIcon from "../../svg/wedding-word.svg";
import BusinessContentWordIcon from "../../svg/business-content-word.svg";
import Head from "next/head";
import {fetchApi} from "../../utilities/fetchApi";
import {MainLayout} from "../../components/Layouts/MainLayout";

const Page = inject('portfolioStore')(observer(props => {
    const {pageData} = props;
    const {videoUrls} = pageData;

    const [swiper, setSwiper] = useState(null);
    useEffect(() => {
        if (swiper !== null) {
            setReachBeginning(swiper.isBeginning);
            setReachEnd(swiper.isEnd);

            swiper.on('slideChange', () => {
                setReachBeginning(swiper.isBeginning);
                setReachEnd(swiper.isEnd);
            });
        }
    }, [swiper]);

    const [reachBeginning, setReachBeginning] = useState(true);
    const [reachEnd, setReachEnd] = useState(false);

    const settingsSlider = {
        getSwiper: setSwiper,
        watchOverflow: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
            dynamicBullets: true,
            dynamicMainBullets: 5,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    };

    const goNext = () => {
        if (swiper !== null) {
            swiper.slideNext();
        }
    };

    const goPrev = () => {
        if (swiper !== null) {
            swiper.slidePrev();
        }
    };

    const handleClickMySlide = (e) => {
        e.stopPropagation();
    };

    return <>
          <Head>
              <title>Портфолио</title>
              <meta name="description"
                    content="Портфолио. Свадебные съемки. Парные, про любовь. Семейные и детские съемки."/>
          </Head>

        <StyledPage>
            <div className="container">
                <div className="links-wrap">
                    <div className="links-wrap__item">
                        <Link href='/portfolio/[category]' as='/portfolio/paired'>
                            <a className='link'>
                                <img src="assets/images/portfolio/paired/poster.jpg" alt="alt"/>
                            </a>
                        </Link>
                        <div className="category-name-wrap">
                            <span className='category-name'><PairedWordIcon/></span>
                        </div>
                    </div>
                    <div className="links-wrap__item">
                        <Link href='/portfolio/[category]' as='/portfolio/wedding'>
                            <a className='link'>
                                <img src="assets/images/portfolio/wedding/poster.jpg" alt="alt"/>
                            </a>
                        </Link>
                        <div className="category-name-wrap">
                            <span className='category-name'><WeddingWordIcon/></span>
                        </div>
                    </div>
                    <div className="links-wrap__item">
                        <Link href='/portfolio/[category]' as='/portfolio/business-content'>
                            <a className='link'>
                                <img src="/assets/images/portfolio/business-content/poster.jpg" alt="alt"/>
                            </a>
                        </Link>
                        <div className="category-name-wrap">
                            <span className='category-name'><BusinessContentWordIcon/></span>
                        </div>
                    </div>
                </div>

                {videoUrls.length !== 0 &&
                <StyledSlider id='portfolio-video' className={
                    videoUrls.length > 1 ? 'offset-indent-anchor' : 'offset-indent-anchor single-slide'}>
                    <Swiper {...settingsSlider}>
                        {videoUrls.map((slide, i) => (
                          <div key={i} className='my-slide' onClick={handleClickMySlide}>
                              <VideoPlayer
                                iconPlay={<PlayIcon/>}
                                videoURL={slide.videoUrl}/>
                              <div className="couple-name">{slide.name}</div>
                          </div>
                        ))}
                    </Swiper>
                    {videoUrls.length > 1 && <>
                        <button
                          disabled={reachBeginning}
                          className="swiper-button-prev"
                          onClick={goPrev}><ArrowIcon/></button>
                        <button
                          disabled={reachEnd}
                          className="swiper-button-next"
                          onClick={goNext}><ArrowIcon/></button>
                    </>}
                </StyledSlider>}
            </div>

            <PreFooterLogoSection/>
        </StyledPage>
      </>
}));

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
    const {appStore} = store;

    const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi('/frontend/portfolio-page')]);

    const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

    if (statusOk) {
        const layoutData = data[0].data || {};
        const pageData = data[1].data;

        appStore.setLayoutData(layoutData);

        return {
            layoutData,
            pageData
        }
    }
};

export default Page;