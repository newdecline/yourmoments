import React, {useEffect} from 'react';
import Link from 'next/link';
import renderHTML from 'react-render-html';
import {inject, observer} from 'mobx-react';
import {Styled, StyledTariffCard} from "../components/StytledPages/PricePage/styled";
import {PreFooterLogoSection} from "../components/PrefooterLogoSection/PreFooterLogoSection";
import {getMaxOfArray} from "../utilities/getMaxOfArray";
import Head from "next/head";
import {fetchApi} from "../utilities/fetchApi";
import {MainLayout} from "../components/Layouts/MainLayout";

const Page = inject('appStore')(observer(props => {
  const {
    pageData
  } = props;

  const tariffs = pageData;

  const alignHeightCards = () => {
    const $innerCards = document.querySelectorAll('.card-wrap .inner');
    const $photoResultsList = document.querySelectorAll('.card-wrap .photo-results-list');

    const dataInnerCards = [];
    const dataPhotoResultsList = [];

    if (window.matchMedia("(min-width: 1024px)").matches) {
      $innerCards.forEach(item => {
        dataInnerCards.push(item.offsetHeight);
      });
      const maxValueDataInnerCards = getMaxOfArray(dataInnerCards);
      $innerCards.forEach(item => {
        item.style.height = `${maxValueDataInnerCards}px`;
      });


      $photoResultsList.forEach(item => {
        dataPhotoResultsList.push(item.offsetHeight);
      });
      const maxValueDataPhotoResultsList = getMaxOfArray(dataPhotoResultsList);

      $photoResultsList.forEach(item => {
        item.style.height = `${maxValueDataPhotoResultsList}px`;
      });
    }
  };

  useEffect(() => {
    alignHeightCards();
  }, []);

  return <>
    <Head>
      <title>Цены</title>
      <meta name="description"
            content="Стоимость наших услуг. Если Вам необходимы услуги одного из нас — Вы можете заказать фото или видеосъемку по отдельности, а также запросить индивидуальный расчет стоимости вашего торжества."/>
    </Head>

    <Styled>
      <div className="container">
        <h6 className="page-title">Стоимость наших услуг</h6>

        <div className="card-wrap">
          {tariffs.map((tariff, i) => {
            return (
              <StyledTariffCard key={i} className={`card-wrap__item-${i + 1}`}>
                <div className="card-notes">
                  <div className="inner">
                    <div className="duration">{tariff.duration}</div>
                    <div className="tariff">{tariff.name}</div>

                    {!!tariff.serviceName1 && <>
                      <div className="name-service">{tariff.serviceName1}</div>
                      <div className="cost">{tariff.cost1}</div>
                      <div className="results-list photo-results-list">
                        {tariff.resultsHTML1 && renderHTML(tariff.resultsHTML1)}
                      </div>
                    </>
                    }

                    {
                      !!tariff.serviceName2 && <>
                        <div className="name-service">{tariff.serviceName2}</div>
                        <div className="cost">{tariff.cost2}</div>
                        <div className="results-list">
                          {tariff.resultsHTML2 && renderHTML(tariff.resultsHTML2)}
                        </div>
                      </>
                    }

                    {
                      !!tariff.surchargeHTML && <>
                        <div className="title-surcharge">Возможные доплаты:</div>
                        <div className="surcharge-list">
                          {renderHTML(tariff.surchargeHTML)}
                        </div>
                      </>
                    }

                    <Link href="/contacts#form"><a className='reserve-date'>Забронировать</a></Link>

                  </div>

                  {!!tariff.notesHTML && renderHTML(tariff.notesHTML)}
                </div>
              </StyledTariffCard>
            )
          })}
        </div>

        <p className="page-note">Если Вам необходимы услуги одного из нас — Вы можете заказать фото или видеосъемку
          по
          отдельности,
          а также запросить индивидуальный расчет стоимости вашего торжества.</p>
      </div>

      <PreFooterLogoSection/>
    </Styled>
  </>
}));

Page.Layout = MainLayout;

Page.getInitialProps = async ({store}) => {
  const {appStore} = store;

  const data = await Promise.all([fetchApi('/frontend/through-elements'), fetchApi('/frontend/price-page')]);

  const statusOk = data.map(item => item.status).filter(item => item !== 200).length === 0;

  if (statusOk) {
    const layoutData = data[0].data || {};
    const pageData = data[1].data;

    appStore.setLayoutData(layoutData);

    return {
      layoutData,
      pageData
    }
  }
};

export default Page;